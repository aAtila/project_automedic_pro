<?php
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
 
    if(isset($_GET['id']) && !empty($_GET['id']) && CTYPE_DIGIT($_GET['id'])) {
        // Save job id
        $id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);
    } else {
        redirect('/admin/jobs/');
    }    

    // Meta/SEO data for the current page
    $pageDetails = [
        'tagline' => 'manage jobs',
        'title' => 'Edit Job'
    ];
    // Include in header
    $headerLinks = array(
        '<link rel="stylesheet" href="/css/bootstrap-multiselect.css" type="text/css"/>'
    );
    // Include in footer
    $footerLinks = array(
        '<script type="text/javascript" src="/js/bootstrap-multiselect.js"></script>'
    );

    // Grab data required for the form

    $services = getServices($pdo);
    $status = getAllStatus($pdo);
    $job = getSingleJob($pdo, $id);

    // Get current status id to compare it later with the submitted one
    $currentStatusId = $job[0]->status_id;

    // Get current service ids to compare it later with the submitted ones
    $currentServices = array();
    foreach($job[1] as $service){
        $currentServices[] = $service->serviceId;
    }



    // Check for POST method
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        // process form

        // Sanitize POST data
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        // Init data
        $data = [
            'status' => trim($_POST['status']),
            'current_status_id' => $currentStatusId,
            'total_price' => trim($_POST['total_price']),
            'total_time' => trim($_POST['total_time']),
            'car' => trim($_POST['car']),
            'email' => $job[0]->email,
            'decline_reason' => (empty(trim($_POST['decline_reason']))) ? false : trim($_POST['decline_reason']),
            'extra_costs' => (empty(trim($_POST['extra_costs']))) ? false : trim($_POST['extra_costs']),
            'extra_costs_reason' => (empty(trim($_POST['extra_costs_reason']))) ? false : trim($_POST['extra_costs_reason']),
            'services' => $_POST['services'],
            'current_services' => $currentServices,
            'notes' => trim($_POST['description']),
            'car_err' => '',
            'total_price_err' => ''
        ];

    // Make sure errors are empty
    if(empty($data['car_err']) && empty($data['total_price_err'])) {
        // Update user
        // dd(array($data, $id));
        if(adminUpdateJob($pdo, $data, $id)) {
            $msg->success('Job updated (<a href="/admin/jobs/show/'.$id.'" class="text-main"><u>see changes</u></a>)', '/admin/jobs/');
        } else {
            die('Something went wrong');
        }
    }

    } else {
        // Init data
        $data = [
            'status' => $job[0]->status_id,
            'total_price' => '',
            'total_time' => '',
            'car' => '',
            'services' => '',
            'current_services' => '',
            'notes' => '',
            'car_err' => '',
            'total_price_err' => '',
            'decline_reason' => false,
            'extra_costs' => false,
            'extra_costs_reason' => false
        ];
    }

    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/header.php');
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/navigation.php');
?>
    <main>
        <div class="container">
            
            <div class="row">
                <!-- Sidebar -->
                <div class="col-md-3 d-none d-md-block">
                    <?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/sidebar.php'); ?>
                </div>
                <div class="col-md-9">
                <!-- Users overview-->
                <div class="card">
                    <div class="card-header main-color-bg"><i class="far fa-edit"></i> Edit job</div>
                        <div class="card-body">
                        <form action="/admin/jobs/edit/<?= $id ?>" method="POST" class="pr-4 pl-4 needs-validation" id="edit-job-form" novalidate>
                            <div class="form-group row">
                                <label for="status" class="col-sm-4 col-form-label">Status:</label>
                                <div class="col-sm-8">
                                <select name="status" id="job-status" class="form-control <?= (!empty($data_sp['status_err'])) ? 'is-invalid' : ''; ?>" value="<?= $job[0]->jobStatus ?>">
                                    <?php foreach($status as $name) : ?>
                                    <option value="<?= $name->id ?>" <?php if(($job[0]->status_id)==($name->id)){echo("selected");}?>><?= $name->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                                </div>
                            </div>
                            <div class="form-group row" id="reason-box" style="display:none;">
                                <label for="email" class="col-sm-4 col-form-label">Reason for Decline:</label>
                                <div class="col-sm-8">
                                <textarea class="form-control" name="decline_reason" rows="5"></textarea>
                            </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-4 col-form-label">Total Price (euros):</label>
                                <div class="col-lg-4 input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">€</div>
                                    </div>
                                    <input type="text" name="total_price" class="form-control <?= (!empty($data['total_price_err'])) ? 'is-invalid' : ''; ?>" value="<?= $job[0]->total_price ?>" id="total_price_field">
                                    <span class="invalid-feedback"><?= $data['total_price_err'] ?></span>
                                </div>
                                <div class="col-lg-4">
                                    <a href="#" class="btn btn-block btn-main" id="extra-costs">Add Extra Costs</a>
                                </div>
                            </div>
                            <div class="form-group row" id="extra-costs-input" style="display:none;">
                                <label for="email" class="col-sm-4 col-form-label">Extra Costs (euros):</label>
                                <div class="col-lg-3 input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">€</div>
                                    </div>
                                    <input name="extra_costs" class="form-control" placeholder="How much?">
                                </div>
                                <div class="col-lg-5">
                                    <input name="extra_costs_reason" class="form-control" placeholder="Reason...">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-4 col-form-label">Total Time Needed (minutes):</label>
                                <div class="col-sm-8">
                                    <input name="total_time" class="form-control <?= (!empty($data_sp['total_time_err'])) ? 'is-invalid' : ''; ?>" value="<?= $job[0]->total_time ?>">
                                    <span class="invalid-feedback"><?= $data['total_time_err'] ?></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-4 col-form-label">Car:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="car" class="form-control <?= (!empty($data['car_err'])) ? 'is-invalid' : ''; ?>" value="<?= $job[0]->plateNumber ?>" id="car_field">
                                    <span class="invalid-feedback"><?= $data['car_err'] ?></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="services" class="col-sm-4 col-form-label">Services:</label>
                                <div class="col-sm-8">
                                <select name="services[]" multiple id="select-services" class="form-control <?= (!empty($data_sp['service_err'])) ? 'is-invalid' : ''; ?>">
                                <?php for($i=0; $i < count($services); $i++) {
                                        echo '<option value="'.$services[$i]->id.'"';
                                        for($j=0; $j < count($job[1]); $j++) {
                                            if ($services[$i]->id ==  $job[1][$j]->serviceId) {
                                                echo ' selected';
                                            }
                                        }
                                        echo '>'.$services[$i]->name.'</option>';
                                }; ?>
                                </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="description" rows="5" placeholder="Tell us as much as you can about the problem.."><?= $job[0]->notes ?></textarea>
                            </div>
                            <div class="row">
                                <div class="col"><input type="submit" value="Update" id="submit-btn" class="btn btn-block main-color-bg"></div>
                                <div class="col"><a class="btn btn-secondary btn-block" href="/admin/jobs/">Back</a></div>
                            </div>
                        </form>
                        </div>
                </div>
            </div>
        </div>
    </main>

<?php include ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/footer.php'); ?>

<script>
    $('#select-services').multiselect({
        buttonWidth: '100%',
        maxHeight: 400,
        
    });

    $('#job-status').on('change',function(){
        if( $(this).val() == 3 ) {
            // $('#reason-box').show()
            $('#reason-box').slideDown('fast');
        } else {
            $('#reason-box').slideUp('fast');
        }
    });

    $('#extra-costs').on('click',function(){
        $('#extra-costs-input').slideToggle('fast');
        // $('#submit-btn')[0].disabled = false;
    });
</script>