$(document).ready(function() {

    // read the value of the cookie (which is cars id)
    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
        }
    
    
    //console.log(readCookie('reservations')); 
    var stringSplit = readCookie('reservations').split(".");
    var myId = stringSplit[0];
    var myDuration = stringSplit[1];
    var jobId = stringSplit[2];
    var currentDate = $('#calendar').fullCalendar('getDate');
    console.log('myId', myId);
    console.log('duration', myDuration);
    console.log('jobId', jobId);

    var calendar = $('#calendar').fullCalendar({
        header:{
            left:'prev, next today',
            center:'title',
            right:'month, agendaWeek, agendaDay'
        },
        // disable scrolling
        // prevent resize of the duration of event           
        eventDurationEditable: false,
        allDayDefault: false,
        aspectRatio: 2,
        height: "auto",             
        navLinks: true, // can click day/week names to navigate views
        // set the master editable property to false, and current to true
        editable: false, // prevent drag and drop
        eventLimit: true, // allow "more" link when too many events
        // select multiple days or hours
        selectable: true,
        selectHelper: true,
        minTime: "7:00",
        maxTime: "19:00",
        allDaySlot: false,
        // don't allow to overlap
        selectOverlap: false,
        // draging no overlap
        eventOverlap: false,   
        slotDuration: "00:30:00",
        // load event        
        events: 'load.php',
        eventColor: 'orange',   
        weekends: false,     

        // this function limits the range for dates
        validRange: function(nowDate) {            
            return {
            start: currentDate,
            end: nowDate.clone().add(6, 'months')
            };
        },
        
        // color and hover
        dayRender: function(date, cell) {
            cell.append("<span class='hoverEffect' style='display:none;'></span>");
            cell.mouseenter(function() {
                cell.find(".hoverEffect").show();
                cell.css("background", "rgba(0,255,0,.3)");
            }).mouseleave(function() {
                $(".hoverEffect").hide();
                cell.removeAttr('style');
            });
        },     

        dayClick: function(date, event, view) {
            if(view.name == 'month') {                
                $('#calendar').fullCalendar('changeView', 'agendaWeek');
                $('#calendar').fullCalendar('gotoDate', date);
            }
        },
        
        select: function(start, end, date, view) {  
            if(view.name != 'month') {           
                end = start.clone().add(myDuration,'minutes');
                var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
                var event = {
                    job_id: jobId,
                    cars_id: myId,
                    start: start,
                    end: end
                    };                              
                var array = calendar.fullCalendar('clientEvents');            
                for(i in array){
                    console.log('event end', event.end);
                    console.log('event start', array[i].start._i);
                    if(array[i].id != event.id){
                        if (event.end > array[i].start._i && event.start < array[i].end._i){
                            console.log('overlap');
                            //$("#cal-overlap-modal").modal('show'); 
                            swal("Please choose another time slot - overlap is not allowed!", {
                                button: "Close",
                              });               
                            //alert("Please choose another time slot - overlap is not allowed!");                        
                            $('#calendar').fullCalendar('unselect');
                            return true;
                        }                
                    }
                }                
                
                swal({
                    title: "You are to set an appointment!",
                    text: "Happy with appointment starting on " + start + " and finishing on " + end, 
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    $.ajax({
                        url: "insert.php",
                        type: "POST",
                        data: {         
                            job_id: jobId,               
                            cars_id: myId,
                            start: start,
                            end: end
                        },                        
                        success: function() {
                            calendar.fullCalendar('refetchEvents');
                            //alert("Added Successfully");                                                     
                            swal("You made an appointment!", {
                                icon: "success"                        
                            }).then(function(result){
                                window.location = "../";
                                           });
                            //window.location.href = "../";
                        }                    
                    });
                        //swal("You made an appointment!", {
                        //icon: "success"                        
                    //});
                    //window.location.href = "../";
                  } else {
                        $('#calendar').fullCalendar('unselect');
                        swal("Choose another time.");
                  }
                });
            }
        }
    });
});
// this in console
//jQuery("#calendar").fullCalendar("clientEvents")
