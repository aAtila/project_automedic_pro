<?php
      $limit = 5;
      /*How many adjacent page links should be shown on each side of the current page link.*/
    
    if(isset($_GET['page']) && $_GET['page'] != "") {
        $page = $_GET['page'];
        $offset = $limit * ($page-1);
      } else {
        $page = 1;
        $offset = 0;
      }

    $paginationData = [
        'limit' => $limit,
        'page' => $page,
        'offset' => $offset
    ];