<?php
    if(!isLoggedIn()) {
        $msg->error('You need to be logged in to access this page.', '/login.php');
    } elseif(isUser()) {
        $msg->error('You don\'t have permissions to access that page.', '/customers/');
    }

    $countRows = countRows($pdo);

    // Get breadcrumb elements
    $url = explode('/', $_SERVER['REQUEST_URI']);
    // Iterate through the array
    $breadcrumb = array_filter( $url, function($entry) {
        if(!empty($entry) && ctype_alpha($entry)) {
            return $entry;
        };
    });
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<?php if(isset($headerLinks)){foreach($headerLinks as $link) {echo '    '.$link."\r\n";}} ?>
    <link rel="stylesheet" href="/css/animate.css">
    <link rel="stylesheet" href="/css/style.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php if(isset($pageDetails)){ echo $pageDetails['title'].' | ';} ?><?= SITENAME ?></title>
</head>
<body>