<div class="list-group mb-3">
    <a href="#" class="list-group-item list-group-item-action active main-color-bg">
    <i class="far fa-clipboard"></i> Overview
    </a>
    <a href="#" class="list-group-item list-group-item-action"> Registered Vehicles <span class="badge badge-secondary badge-pill float-right">1</span></a>
    <a href="#" class="list-group-item list-group-item-action"> Vehicles on Service <span class="badge badge-secondary badge-pill float-right">2</span></a>
    <a href="#" class="list-group-item list-group-item-action"> Finished Repairs <span class="badge badge-secondary badge-pill float-right">3</span></a>
</div>