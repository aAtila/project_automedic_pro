<?php
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');

    if(isset($_GET['id']) && !empty($_GET['id']) && CTYPE_DIGIT($_GET['id'])) {

        $pageDetails = [
            'tagline' => 'User details',
            'title' => 'user Details'
        ];

        $id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT); // Set user id variable
        $user = getSingleUser($pdo, $id);
        if(!$user) {
            echo 'No such user';
        } else {
            require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/header.php');
            require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/navigation.php');
            ?>

            <main>
                <div class="container">
                    <div class="row">
                        <!-- Sidebar -->
                        <div class="col-md-3 d-none d-md-block">
                            <?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/sidebar.php'); ?>
                        </div>
                        <div class="col-md-9">
                            <!-- Job details-->
                            <div class="card">
                                <div class="card-header main-color-bg"><i class="fas fa-wrench"></i> User details</div>
                                    <div class="card-body">
                                        <!-- Show flash message -->
                                        <?php $msg->display() ?>
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <h2>
                                                    <small class="text-muted">name</small>
                                                    <strong><?= $user->name ?></strong>
                                                </h2>
                                            </div>
                                            <div>
                                                <a href="/admin/users/edit/<?= $user->userId ?>" class="btn btn-main">Edit User</a>
                                            </div>
                                        </div>
                                        <!-- Details card -->
                                        <div class="container-fluid bg-light mt-2 pb-3 border">
                                            <div class="mt-3">
                                                <div class="row" style="line-height: 1.8">
                                                    <div class="col-md-3"><strong>Status:</strong></div>
                                                    <div class="col-md-9">
                                                        <button class="btn btn-main btn-sm disabled"><?= ($user->active === 1) ? 'Active' : 'Inactive'; ?></button>
                                                    </div>

                                                    <div class="col-md-3"><strong>Email:</strong></div>
                                                    <div class="col-md-9"><?= $user->email ?></div>

                                                    <div class="col-md-3"><strong>Role:</strong></div>
                                                    <div class="col-md-9"><?= $user->role ?></div>

                                                    <div class="col-md-3"><strong>Registration Date:</strong></div>
                                                    <div class="col-md-9"><?= date("m/d/Y h:i A", strtotime($user->created_at)) ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- .card-body end -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>

            <?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/footer.php'); ?>

    <?php }} else {
        // redirect('/users/');
        echo 'better luck next time';
    }
?>