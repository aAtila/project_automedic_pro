<?php

    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');

    if(isset($_GET['id']) && !empty($_GET['id']) && CTYPE_DIGIT($_GET['id'])) {
        $id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);
        if(adminDeleteCar($pdo, $id)) {
            $msg->success('Car and all associated data deleted.', '/admin/cars/');
        } else {
            die('Something went wrong!');
        }
    } else {
        redirect('/admin/cars/');
    }

?>