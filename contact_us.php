<?php

    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
    // Check if $_POST is set
    if (empty($_POST)) {
    redirect('/404.php');
    exit;
    }
    // Init data
    $data = [
        'name' => $_POST['name'],
        'email' => $_POST['email'],
        'subject' => $_POST['subject'],
        'message' => nl2br(htmlentities($_POST['message'], ENT_QUOTES, 'UTF-8')),
        'ip_address' => filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP),
        'browser' => $_SERVER['HTTP_USER_AGENT']
    ];

    $contactMail = sendContactMail($data);

    if ($contactMail === TRUE) {
        echo '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
            Email successfully sent!
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            ';
    } else {
        echo '
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
        Sorry, something went wrong. Please give it another try or mail us directly at <i>hello@automedic.icu</i> 
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        ';
    }