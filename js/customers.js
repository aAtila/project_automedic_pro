$(document).ready(function(){

    // Show Modals
   $('#check-status-btn').click(function() {
       $('#check-status-modal').modal('show');
   });

   $('#submit-problem-btn').click(function() {
        $('#submit-problem-modal').modal('show');
   });

   $('#new-vehicle-btn').click(function() {
       $('#new-vehicle-modal').modal('show');
   });

   // Submit Forms Using jQuery and AJAX

   // Search Status Form
   $('#search-status-form').submit(function(event){

        // Stop form from submitting normally
        event.preventDefault();
        
        // Get values from form
        var formData = {"plate_number" : $('#plate-number-field').val()}
        //console.log(formData);
        // Process AJAX request
        $.post('/customers/process_status.php', formData, function(data) {
        // Append data into #results div
        $('#results').html(data)
        //      With JSON
            var newData = JSON.parse(data);
             $('#results').html(
            '<div class="alert alert-success" role="alert">'+
            '<h4 class="alert-heading">Car found!</h4>'+
            '<hr>'+
            '<p>Plate number: '+ formData['plate_number']+'</p>'+
            '<p>Current Status: '+ newData.statusName+'</p>'+
            '<p>Total price (Euros): '+ newData.totalPrice+'</p>'+
            '<p>Time of update: '+ newData.updatedAt+'</p>'+
            '<p>Note: ' + newData.jobNote+'</p>' +
            '</div>'
           );
       });

       // Hide modal
       $('#check-status-modal').modal('hide');
   });
});
