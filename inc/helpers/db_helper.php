<?php

// login.php
    function loginUser($pdo, $email, $pass) {
        $sql = 'SELECT * from users WHERE email = :email';
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['email' => $email]);

        $row = $stmt->fetch();

        $hashed_password = $row->password;
        if(password_verify($pass, $hashed_password)) {
            return $row;
        } else {
            return false;
        }
    }

// register.php
    function registerUser($pdo, $data) {
        $sql = 'INSERT INTO users (name, email, password, hash, expires) VALUES (:name, :email, :password, :hash, :expires)';
        $stmt = $pdo->prepare($sql);
        if($stmt->execute(['name' => $data['name'], 'email' => $data['email'], 'password' => $data['password'], 'hash' => $data['hash'], 'expires' => $data['expires']])){
            return true;
        } else {
            return false;
        }
    }

// verify.php
    function matchUserEmailAndHash($pdo, $email, $hash) {
        $sql = 'SELECT * from users WHERE email = :email AND hash = :hash';
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['email' => $email, 'hash' => $hash]);

        $row = $stmt->fetch();

        // query if active
        $sql2 = 'SELECT hash from users WHERE email = :email';
        $stmt2 = $pdo->prepare($sql2);
        $stmt2->execute(['email' => $email]);

        $hash_value = $stmt2->fetchColumn();
        //ChromePhp::log($hash_value);
        // Check row
        if($stmt->rowCount() > 0) {
            return array(true, $row);
        // check if user is already active
        } elseif ($hash_value === '1'){
            return array(false, 'active');
        } else {
            return false;
        }
    }
    
    // sat hash to '1' for user is already active
    function activateUser($pdo, $email, $hash) {
        $sql = 'UPDATE users SET active = 1, hash = "1", expires = 0 WHERE email = :email AND hash = :hash';
        $stmt = $pdo->prepare($sql)->execute(['email' => $email, 'hash' => $hash]);
    }

    function deleteInactiveUser($pdo, $email) {
        $sql = 'DELETE FROM users WHERE email = :email AND expires < :time';
        $stmt = $pdo->prepare($sql)->execute(['email' => $email, 'time' => time()]);
    }

// reset.php
    function verifyTokens($pdo, $data) {
        $sql = 'SELECT * FROM password_reset WHERE selector = :selector AND expires >= :time';
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['selector' => $data['selector'], 'time' => time()]);
        $row = $stmt->fetch();
        return $row;

    }

    function updatePassword($pdo, $password, $id) {
        $sql = 'UPDATE users SET password = :password WHERE id = :id';
        $stmt = $pdo->prepare($sql)->execute(['password' => $password, 'id' => $id]);
    }

// reset_password.php
    function insertResetToken($pdo, $data) {
        $sql = 'INSERT INTO password_reset (email, selector, token, expires) VALUES (:email, :selector, :token, :expires)';
        $stmt = $pdo->prepare($sql)->execute(['email' => $data['email'], 'selector' => $data['selector'], 'token' => $data['token'], 'expires' => $data['expires']]);
    }

// reset.php && reset_password.php
    function deleteExistingToken($pdo, $email) {
        $sql = 'DELETE FROM password_reset WHERE email = :email';
        $stmt = $pdo->prepare($sql)->execute(['email' => $email]);
    }

// login.php && register.php && reset.php
// admin/users/add.php && edit.php
    function findUserByEmail($pdo, $email) {
        $sql = 'SELECT * from users WHERE email = :email';
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['email' => $email]);
        
        $row = $stmt->fetch();

        // Check row
        if($stmt->rowCount() > 0) {
            return array(true, $row);
        } else {
            return false;
        }
    }

// admin/users/index.php
    function getAllUsers($pdo) {
        $stmt = $pdo->query('SELECT *,
                             users.id as userId,
                             roles.id as roleId
                             FROM users
                             INNER JOIN roles ON users.role_id = roles.id
                             ORDER BY users.role_id ASC');

        $result = $stmt->fetchAll();
        return $result;
    }

// admin/users/show.php && edit.php
// customers/show.php
    function getSingleUser($pdo, $id) {
        $sql = 'SELECT *,
                users.id as userId,
                roles.id as roleId
                FROM users
                INNER JOIN roles ON users.role_id = roles.id
                WHERE users.id = :userid;';

        $stmt = $pdo->prepare($sql);
        $stmt->execute(['userid' => $id]);
        $row = $stmt->fetch();
        return $row;
    }

// admin/users/add.php && edit.php
    function getUserRoles($pdo) {
        $stmt = $pdo->query('SELECT * FROM roles');
        $result = $stmt->fetchAll();
        return $result;
    }

// admin/users/add.php
    function adminAddNewUser($pdo, $data) {
        $sql = 'INSERT INTO users (name, email, password, active, role_id) VALUES (:name, :email, :password, 1, :roleid)';
        $stmt = $pdo->prepare($sql);
        if($stmt->execute(['name' => $data['name'], 'email' => $data['email'], 'password' => $data['password'], 'roleid' => $data['role']])) {
            return true;
        } else {
            return false;
        }
    }

// admin/users/delete.php
    function adminDeleteUser($pdo, $id) {
        $sql = 'DELETE t1, t2, t3, t4 FROM users AS t1        
                LEFT JOIN  cars AS t2 ON t1.id = t2.user_id
                LEFT JOIN  jobs AS t3 ON t1.id = t3.user_id
                LEFT JOIN  job_service AS t4 ON t3.id = t4.job_id
                WHERE  t1.id = :id';
        //$sql = 'DELETE FROM users WHERE id = :id';
        if ($stmt = $pdo->prepare($sql)->execute(['id' => $id])) {
            return true;
        } else {
            return false;
        }
    }

// admin/users/edit.php
    function adminUpdateUser($pdo, $data, $id) {
        $sql = 'UPDATE users SET name = :name, email = :email, role_id = :roleid, active = :active WHERE id = :id';
        if($stmt = $pdo->prepare($sql)->execute(['name' => $data['name'], 'email' => $data['email'], 'roleid' => $data['role'], 'active' => $data['active'], 'id' => $id])) {
            return true;
        } else {
            return false;
        }
    }

// admin/jobs/delete.php
function adminDeleteJob($pdo, $id) {
    $sql = 'DELETE t1, t2 FROM jobs AS t1            
            LEFT JOIN  job_service AS t2 ON t1.id = t2.job_id            
            WHERE  t1.id = :id';

//    $sql = 'DELETE FROM jobs WHERE id = :id';
    if ($stmt = $pdo->prepare($sql)->execute(['id' => $id])) {
        return true;
    } else {
        return false;
    }
}

// customers/index.php
    function userHasCar($pdo, $id) {
        $sql = 'SELECT * from cars WHERE user_id = :id';
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['id' => $id]);
        // Check row
        if($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function findPlateNumber($pdo, $num) {
        $sql = 'SELECT * from cars WHERE plate_number = :platenumber';
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['platenumber' => $num]);
        
        $row = $stmt->fetch();
    
        // Check row
        if($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function addNewVehicle($pdo, $data) {
        $sql = 'INSERT INTO cars (plate_number, brand, model, year, user_id) VALUES (:platenumber, :brand, :model, :year, :userid)';
        $stmt = $pdo->prepare($sql);
        if($stmt->execute(['platenumber' => $data['plate_number'], 'brand' => $data['brand'], 'model' => $data['model'], 'year' => $data['year'], 'userid' => $data['user_id']])) {
            return true;
        } else {
            return false;
        }
    }

    function submitProblem($pdo, $data) {
        $sql = 'INSERT INTO jobs (user_id, car_id, note) VALUES (:userid, :carid, :note)';
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['userid' => $data['user_id'], 'carid' => $data['car_id'], 'note' => $data['description']]);

        // Get id of the created job
        $job_id = $pdo->lastInsertId();

        // Set job id in session
        $_SESSION['lastId'] = $job_id;

        // Add services
        $sql2 = 'INSERT INTO job_service (job_id, service_id) VALUES (:jobid, :serviceid)';
        $stmt2 = $pdo->prepare($sql2);

        foreach($data['services'] as $service) {
             $stmt2->execute(['jobid' => $job_id, 'serviceid' => $service]);
        }

        return true;
    }

    function getServices($pdo) {
        $stmt = $pdo->query('SELECT * FROM services');
        $result = $stmt->fetchAll();
        return $result;
    }

    function getUserVehicleAll($pdo, $id) {
        $sql = 'SELECT * from cars WHERE user_id = :id';
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['id' => $id]);
        $result = $stmt->fetchAll();

        // Check row
        if($stmt->rowCount() > 0) {
            return $result;
        } else {
            return false;
        }
    }

// customers/process_status.php
    function itsUsersCar($pdo, $data) {
        $sql = 'SELECT * from cars WHERE user_id = :id AND plate_number = :platenumber';
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['id' => $data['user_id'], 'platenumber' => $data['plate_number']]);

        $row = $stmt->fetch();

        // Check row
        if($stmt->rowCount() > 0) {
            return array(true, $row);
        } else {
            return false;
        }
    }

    function getVehicleStatus($pdo, $data, $car) {
        $sql = 'SELECT *,
        jobs.id as jobId,
        jobs.car_id as carId,
        status.name as statusName,
        jobs.note as jobNote,
        jobs.total_price as totalPrice,
        jobs.updated_at as updatedAt
        FROM jobs
        INNER JOIN status ON jobs.status_id = status.id        
        WHERE jobs.user_id = :userid AND jobs.car_id = :carid;';

        $stmt = $pdo->prepare($sql);
        $stmt->execute(['userid' => $data['user_id'], 'carid' => $car]);
        $row = $stmt->fetch();

        // Check row
        if($stmt->rowCount() > 0) {
            return $row;
        } else {
            return false;
        }
    }

    function getAllJobs($pdo) {
        $sql = 'SELECT
        jobs.id as JobId,
        users.name as userName,
        cars.plate_number as plateNumber,
        jobs.note as notes,
        jobs.created_at as dateCreated,
        status.name as jobStatus,
        cars.brand as carBrand,
        cars.model as carModel,
        cars.year as carYear
        FROM jobs
        INNER JOIN users on jobs.user_id = users.id
        INNER JOIN cars on jobs.car_id = cars.id
        INNER JOIN status on jobs.status_id = status.id';

        $stmt = $pdo->prepare($sql);
        $stmt->execute();        
        $row = $stmt->fetchAll();
        $rowCount = $stmt->rowCount();
        $result = array($rowCount, $row);
        return $result;        
    }

    function getSingleJob($pdo, $id) {
        $sql = 'SELECT *,
        jobs.id as jobId,
        users.id as userId,
        users.name as userName,
        cars.plate_number as plateNumber,
        cars.id as carId,
        jobs.note as notes,
        jobs.created_at as dateCreated,
        jobs.updated_at as lastUpdated,
        status.name as jobStatus,
        cars.brand as carBrand,
        cars.model as carModel,
        cars.year as carYear,
        jobs.total_price,
        jobs.total_time
        FROM jobs
        INNER JOIN users on jobs.user_id = users.id
        INNER JOIN cars on jobs.car_id = cars.id
        INNER JOIN status on jobs.status_id = status.id
        WHERE jobs.id = :id';

        $stmt = $pdo->prepare($sql);
        $stmt->execute(['id' => $id]);
        if($stmt->rowCount() > 0) {
            $rowJob = $stmt->fetch();
            // Get job's services
            $sql = 'SELECT services.name as services, services.id as serviceId FROM services INNER JOIN job_service on services.id = job_service.service_id WHERE job_service.job_id = :id';
            $stmt = $pdo->prepare($sql);
            $stmt->execute(['id' => $id]);
            $rowServices = $stmt->fetchAll();
            // Return job details with services
            $result = array($rowJob, $rowServices);
            return $result;
        } else {
            return false;
        }
    }

    function getPendingJobs($pdo, $data) {
        $sql = "SELECT
        jobs.id as JobId,
        users.name as userName,
        cars.plate_number as plateNumber,
        jobs.note as notes,
        jobs.created_at as dateCreated,
        cars.brand as carBrand,
        cars.model as carModel,
        cars.year as carYear
        FROM jobs
        INNER JOIN users on jobs.user_id = users.id
        INNER JOIN cars on jobs.car_id = cars.id
        WHERE jobs.status_id = 1
        LIMIT :offset, :limit";

        $stmt = $pdo->prepare($sql);
        $stmt->execute(['offset' => $data['offset'], 'limit' => $data['limit']]);

        // Get all rows
        if($stmt->rowCount() > 0) {
            $row = $stmt->fetchAll();
        } else {
            return false;
        }

        // Count total number of rows in jobs table
        $count_sql = "SELECT * FROM jobs WHERE status_id = 1";
        $stmt = $pdo->prepare($count_sql);
        $stmt->execute();
        $rowCount = $stmt->rowCount();

        // calculate the pagination number by dividing total number of rows with per page.
        $paginations = ceil($rowCount / $data['limit']);

        $result = array(
            $row,
            $rowCount,
            $paginations
        );

        return $result;
    }

    function countRows($pdo) {
        // Count customers
        $stmt = $pdo->query('SELECT COUNT(*) as customers
                             FROM users
                             WHERE role_id = 2');

        $customers = $stmt->fetch();

        // Count users
        $stmt = $pdo->query('SELECT COUNT(*) as users
                             FROM users');

        $users = $stmt->fetch();

        // Count cars
        $stmt = $pdo->query('SELECT COUNT(*) as cars
                             FROM cars');

        $cars = $stmt->fetch();

        // Count open jobs
        $stmt = $pdo->query('SELECT COUNT(*) as openJobs
                             FROM jobs
                             WHERE status_id != 0
                             AND status_id != 1
                             AND status_id != 4
                             ');

        $openJobs = $stmt->fetch();

        // Count all jobs
        $stmt = $pdo->query('SELECT COUNT(*) as jobs from jobs');

        $jobs = $stmt->fetch();

        $result = array($customers, $users, $cars, $openJobs, $jobs);
        return $result;
    }

    function getAllStatus($pdo) {
        $stmt = $pdo->query('SELECT * FROM status');
        if($stmt->rowCount() > 0) {
            $result = $stmt->fetchAll();
            return $result;
        } else {
            return false;
        }
    }

    // admin/cars/index.php
    function getAllCars($pdo) {
        $stmt = $pdo->query('SELECT cars.*, users.name,
                            cars.id as carsId,
                            users.id as userId
                            FROM cars
                            LEFT JOIN users
                            ON cars.user_id = users.id
                            ORDER BY users.name ASC');
        $result = $stmt->fetchAll();
        return $result;
    }

    // admin/cars/edit.php
    function getSingleCar($pdo, $id) {
        $stmt = $pdo->prepare('SELECT cars.*, users.name,
                            cars.id as carsId,
                            users.id as userId
                            FROM cars
                            LEFT JOIN users
                            ON cars.user_id = users.id
                            WHERE cars.id = :carid');
        $stmt->execute(['carid' => $id]);
        $row = $stmt->fetch();
        return $row;
    }    

    // admin/cars/delete.php
    function adminDeleteCar($pdo, $id) {
        $sql = 'DELETE t1, t2, t3 FROM cars AS t1
                LEFT JOIN  jobs AS t2 ON t1.id = t2.car_id
                LEFT JOIN  job_service AS t3 ON t2.id = t3.job_id
                WHERE  t1.id = :id';
        //$sql = 'DELETE FROM cars WHERE id = :id';
        if ($stmt = $pdo->prepare($sql)->execute(['id' => $id])) {
            return true;
        } else {
            return false;
        }
    }

    // see if plate number already exists in db
    // admin/cars/edit.php
    function checkDuplicatePlateNumber($pdo, $platenumber) {
        $sql = 'SELECT * from cars WHERE plate_number = :plate_number';
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['plate_number' => $platenumber]);

        $row = $stmt->fetch();
        //var_dump('hererow', $row);
        // Check row
        
        if($stmt->rowCount() > 0) {
            return array(true, $row);
        } else {
            return false;
        }
    }

    // admin/cars/edit.php
    function adminUpdateCar($pdo, $data, $id) {
        //var_dump($data);
        $sql = 'UPDATE cars SET plate_number = :plate_number, brand = :brand, model = :model, year = :year WHERE id = :id';
        if($stmt = $pdo->prepare($sql)->execute(['plate_number' => $data['plate_number'], 'brand' => $data['brand'], 'model' => $data['model'], 'year' => $data['year'], 'id' => $id])) {
            return true;
        } else {
            return false;
        }
    }

    function adminUpdateJob($pdo, $data, $id) {
        $sql = 'UPDATE jobs SET status_id = :statusid, note = :notes, total_price = :totalprice, total_time = :totaltime WHERE id = :id';
        if($stmt = $pdo->prepare($sql)->execute(['statusid' => $data['status'], 'notes' => $data['notes'], 'totalprice' => $data['total_price'], 'totaltime' => $data['total_time'], 'id' => $id])) {

            // Check if declined and add reason to notes
            if($data['status'] == 3 && $data['current_status_id'] != 3) {
                $sql = "UPDATE jobs SET note = CONCAT('Declined@', NOW(), ' ----- ', 'Reason: ', :decline_reason, '\n\n', note) WHERE id = :id";
                $stmt = $pdo->prepare($sql);
                $stmt->execute(['decline_reason' => $data['decline_reason'], 'id' => $id]);
                sendDeclineMessage($data);
            }
            // Check for extra cost and add reason to notes
            if($data['extra_costs'] !== false) {
                $newPrice = $data['total_price'] + $data['extra_costs'];
                $sql = "UPDATE jobs SET
                        total_price = :newprice,
                        note = CONCAT('Extra costs of (', :extra_costs, ') added@', NOW(), ' ----- ', 'Reason: ', :extra_costs_reason,'\n(Total price was:',:total_price, ')\n\n', note)
                        WHERE id = :id";
                $stmt = $pdo->prepare($sql);
                $stmt->execute(['newprice' => $newPrice,'extra_costs_reason' => $data['extra_costs_reason'], 'total_price' => $data['total_price'], 'extra_costs' => $data['extra_costs'], 'id' => $id]);
            }
            // Update services
            // Check for difference
            if($data['services'] != $data['current_services']){
                // Delete existing services
                $sql = "DELETE FROM job_service WHERE job_id = :id";
                $stmt = $pdo->prepare($sql);
                $stmt->execute(['id' => $id]);

                // Add new services
                $sql = "INSERT INTO job_service (job_id, service_id) VALUES (:jobid, :serviceid)";
                $stmt = $pdo->prepare($sql);

                foreach($data['services'] as $service) {
                    $stmt->execute(['jobid' => $id, 'serviceid' => $service]);
               }
            }

            return true;
        } else {
            return false;
        }
    }

    function getDuration($pdo, $last_id) {
        $sql = 'SELECT total_time from jobs WHERE id = :lastId';
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['lastId' => $last_id]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        return $row['total_time'];
    }

    function addDurationPerJob($pdo, $last_id) {
        $variable = 'SELECT SUM(services.duration) from job_service INNER JOIN services ON job_service.service_id = services.id WHERE job_id = :lastjobid';        
        $stmt = $pdo->prepare($variable);
        $stmt->execute(['lastjobid' => $last_id]);
        $result = $stmt->fetch(PDO::FETCH_NUM);
        $duration = (int)$result[0];

        $sql = 'UPDATE jobs SET total_time = :totalTime WHERE id = :lastId';
        $stmt2 = $pdo->prepare($sql);
        $stmt2->execute(['totalTime' => $duration, 'lastId' => $last_id]);

        //return (int)$result[0]; 
        return $duration;
    }

    function addPricesPerJob($pdo, $last_id) {
        $variable = 'SELECT SUM(services.price) from job_service INNER JOIN services ON job_service.service_id = services.id WHERE job_id = :lastjobid';
        $stmt = $pdo->prepare($variable);
        $stmt->execute(['lastjobid' => $last_id]);
        $result = $stmt->fetch(PDO::FETCH_NUM);
        $price = (int)$result[0];

        $sql = 'UPDATE jobs SET total_price = :totalPrice WHERE id = :lastId';
        $stmt2 = $pdo->prepare($sql);
        $stmt2->execute(['totalPrice' => $price, 'lastId' => $last_id]);

        //return (int)$result[0]; 
        //return $price;
        return true;
    }

    function getAllServices($pdo) {
        $stmt = $pdo->query("SELECT * FROM services WHERE name != 'Consultation'");
        $result = $stmt->fetchAll();
        return $result;
    }

    function pullJobDetails($pdo, $data) {
        // Get plate number
        $sql = 'SELECT plate_number FROM cars WHERE id = :carid';
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['carid' => $data['car_id']]);
        $row = $stmt->fetch();
        $result['plate_number'] = $row->plate_number;

        // Get service names
        $sql = 'SELECT name FROM services WHERE id = :serviceid';
        $stmt = $pdo->prepare($sql);

        foreach($data['services'] as $service) {
            $stmt->execute(['serviceid' => $service]);
            $row = $stmt->fetch();
            $result['services'][] = $row->name;
       }
       return $result;
    }

    function checkDuplicateJob($pdo, $id) {
        $sql = 'SELECT * FROM jobs WHERE car_id = :carid AND status_id != 4 AND status_id != 3';
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['carid' => $id]);
        
        $row = $stmt->fetchAll();

        // check row
        if($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }