<?php

    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
    // Check if $_POST is set
    if (empty($_POST)) {
        $msg->error('You really  don\'t have a reason to access that page.', '/admin/jobs/');
    exit;
    }
    // Check for proper permissions
    if(!isLoggedIn()) {
        $msg->error('You need to be logged in to access this page.', '/login.php');
    } elseif(isUser()) {
        $msg->error('You don\'t have permissions to access that page.', '/customers/');
    }

    // Process form

    // Sanitize POST data
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    // Init data
    $data = [
        'name' => $_POST['name'],
        'email' => $_POST['email'],
        'subject' => $_POST['subject'],
        'message' => nl2br(htmlentities($_POST['message'], ENT_QUOTES, 'UTF-8')),
        'car' => $_POST['car'],
        'plate_number' => filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP)
    ];

    $customerMessage = messageToCustomer($data);

    if ($customerMessage === TRUE) {
        echo '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
            Message successfully sent!
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            ';
    } else {
        echo '
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
        Sorry, something went wrong.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        ';
    }