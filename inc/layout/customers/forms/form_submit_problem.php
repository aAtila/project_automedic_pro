<?php
    // Get services for the form
    $services = getServices($pdo);

    // Get user's existing cars
    $id = $_SESSION['user_id'];
    $usersCars = getUserVehicleAll($pdo, $id);
?>

<form class="pr-4 pl-4 needs-validation" id="submit-problem-form" action="/customers/" method="POST" novalidate>
    <div class="form-group">
        <select name="car_id" class="form-control <?= (!empty($data_sp['car_id_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data_sp['car_id'] ?>">
            <option value="" selected>Choose a car</option>
            <?php foreach($usersCars as $car) : ?>
            <option value="<?= $car->id ?>"><?= $car->plate_number ?></option>
            <?php endforeach; ?>
        </select>
        <span class="invalid-feedback"><?= $data_sp['car_id_err'] ?></span>
    </div>
    <div class="form-group p-3 bg-light rounded text-secondary">
    <?php foreach($services as $service) : ?>
    <div class="form-check">
        <input type="checkbox" class="form-check-input" name="services[]" id="service-<?= $service->id ?>" value="<?= $service->id ?>">
        <label class="form-check-label" for="service-<?= $service->id ?>"><?= $service->name ?> (<?= $service->duration ?> minutes): €<?= $service->price ?></label>
    </div>
    <?php endforeach; ?>
    <small id="emailHelp" class="form-text text-muted pt-1">(choose one or more services...)</small>
    </div>
    <div class="form-group">
        <textarea class="form-control" name="description" rows="5" placeholder="Tell us as much as you can about the problem.."></textarea>
    </div>
    <input type="hidden" name="form_name" readonly value="submit-problem-form">
    <div class="row">
        <div class="col"><input type="submit" value="Submit" class="btn btn-block main-color-bg"></div>
        <div class="col"><button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Close</button></div>
    </div>
</form>