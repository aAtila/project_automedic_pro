<?php
require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');

if(!isLoggedIn()) {
    $msg->error('You need to be logged in to access this page.', '/login.php');
} elseif(isUser()) {
    $msg->error('You don\'t have permissions to access that page.', '/customers/');
}

//update.php
//$connect = new PDO('mysql:host=localhost;dbname=automedic', 'root', '123456');

if(isset($_POST["id"])) {
    $query = "
        UPDATE jobs 
        SET reservation_start = :start, reservation_end=:end 
        WHERE id=:jobid
        ";
        $statement = $connect->prepare($query);
        $statement->execute(
        array(    
            ':jobid'   => $_POST['id'],
            ':start' => $_POST['start'],
            ':end' => $_POST['end'],
            //':cars_id' => $_POST['cars_id'],            
        )
    );    
}
?>
