-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 25, 2018 at 03:33 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `automedic`
--

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id` int(11) NOT NULL,
  `plate_number` varchar(10) NOT NULL,
  `brand` varchar(60) NOT NULL,
  `model` varchar(90) NOT NULL,
  `year` year(4) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id`, `plate_number`, `brand`, `model`, `year`, `user_id`) VALUES
(1, 'SU123AB', 'Fiat', '500c', 2017, 2),
(2, 'SU456CD', 'Tesla', 'Model X', 2018, 4),
(3, 'SU789EF', 'VW', 'Bora', 2003, 5),
(4, 'SU111AA', 'fiat', 'xsd300', 0000, 4),
(5, 'SU609OQ', 'jaguar', 'i-pace', 2018, 4);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reservation_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL DEFAULT '9',
  `status_id` tinyint(1) NOT NULL DEFAULT '1',
  `note` text NOT NULL,
  `total_price` float NOT NULL,
  `total_time` int(5) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reservation_start` datetime NOT NULL,
  `reservation_end` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `user_id`, `reservation_id`, `car_id`, `service_id`, `status_id`, `note`, `total_price`, `total_time`, `created_at`, `updated_at`, `reservation_start`, `reservation_end`) VALUES
(13, 4, 0, 2, 9, 1, 'a', 0, 60, '2018-07-20 14:04:14', '2018-07-25 03:33:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 4, 0, 2, 9, 1, '', 0, 60, '2018-07-20 14:04:34', '2018-07-25 03:33:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 4, 0, 2, 9, 1, 'a', 0, 0, '2018-07-20 14:13:05', '2018-07-25 03:33:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 4, 0, 2, 9, 1, 'a', 0, 0, '2018-07-20 14:14:23', '2018-07-25 03:33:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 4, 0, 2, 9, 1, '', 0, 0, '2018-07-20 14:42:58', '2018-07-25 03:33:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 4, 0, 2, 9, 1, '', 0, 0, '2018-07-20 14:44:06', '2018-07-25 03:33:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 4, 0, 2, 9, 1, '', 0, 0, '2018-07-20 14:46:05', '2018-07-25 03:33:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 4, 0, 5, 9, 1, 'new', 75, 60, '2018-07-24 12:41:22', '2018-07-25 03:33:32', '2018-07-24 08:30:00', '2018-07-24 09:30:00'),
(25, 4, 0, 4, 9, 1, 'two', 110, 120, '2018-07-24 12:42:06', '2018-07-25 03:33:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 4, 0, 4, 9, 1, 'two', 100, 90, '2018-07-24 12:42:57', '2018-07-25 03:33:32', '2018-07-24 09:30:00', '2018-07-24 11:00:00'),
(27, 4, 0, 4, 9, 1, 'three', 350, 240, '2018-07-24 12:44:30', '2018-07-25 03:33:32', '2018-07-25 08:30:00', '2018-07-25 12:30:00'),
(29, 4, 0, 5, 9, 1, 'This is only a test.', 505, 420, '2018-07-24 14:19:02', '2018-07-25 03:33:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 4, 0, 5, 9, 2, 'dfdfdfdsfsdfsdf', 180, 300, '2018-07-24 22:24:25', '2018-07-25 03:33:32', '2018-07-25 12:30:00', '2018-07-25 17:30:00'),
(31, 4, 0, 5, 9, 1, 'dsdsdsdsd', 490, 330, '2018-07-25 01:05:49', '2018-07-25 03:31:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `job_service`
--

CREATE TABLE `job_service` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_service`
--

INSERT INTO `job_service` (`id`, `job_id`, `service_id`) VALUES
(16, 13, 1),
(17, 14, 1),
(18, 15, 2),
(19, 16, 2),
(20, 17, 2),
(21, 18, 2),
(22, 19, 2),
(23, 20, 2),
(24, 21, 2),
(25, 22, 2),
(26, 23, 4),
(27, 23, 2),
(28, 23, 1),
(29, 24, 1),
(30, 25, 1),
(31, 25, 3),
(32, 26, 1),
(33, 26, 2),
(34, 27, 8),
(39, 29, 1),
(40, 29, 2),
(41, 29, 6),
(42, 29, 8),
(43, 30, 1),
(44, 30, 2),
(45, 30, 3),
(46, 30, 7),
(47, 30, 9),
(48, 31, 1),
(49, 31, 4),
(50, 31, 8),
(51, 32, 1),
(52, 32, 4),
(53, 32, 7),
(54, 33, 1),
(55, 33, 4),
(56, 33, 7),
(57, 34, 1),
(58, 34, 4),
(59, 34, 7),
(60, 35, 1),
(61, 35, 4),
(62, 35, 7),
(63, 36, 1),
(64, 36, 4),
(65, 36, 7),
(66, 37, 1),
(67, 37, 4),
(68, 37, 7);

-- --------------------------------------------------------

--
-- Table structure for table `password_reset`
--

CREATE TABLE `password_reset` (
  `ID` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `selector` char(16) DEFAULT NULL,
  `token` char(64) DEFAULT NULL,
  `expires` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `password_reset`
--

INSERT INTO `password_reset` (`ID`, `email`, `selector`, `token`, `expires`) VALUES
(4, 'jane@doe.com', '4526769fa96959e5', '198b0509d236c395b8bb824de0b2eda227620c1905311d63f168df18c0d6838d', 1530236166);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `role` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`) VALUES
(1, 'admin'),
(2, 'customer');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `duration` int(5) NOT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `price`, `duration`, `description`, `image`) VALUES
(1, 'Brakes & Brakes Repair', 75, 60, 'Brake maintenance is crucial to keeping your vehicle operating safely.', 'break-repairs.jpg'),
(2, 'Oil Change', 25, 30, 'Regularly changing your oil and filter will help the engine work its best.', 'oil-change.jpg'),
(3, 'Tires & Tires Repair', 35, 60, 'Regularly inspect and service your tires to prevent a blowout, a flat, or a costly accident.', 'tires.jpg'),
(4, 'Mufflers & Exhaust Systems', 65, 30, 'Mufflers do more than keep your car quiet.', 'mufflers-exhaust.jpg'),
(5, 'Batteries', 45, 30, 'A car battery is either good or it\'s dead, especially during winter when your battery is most likely to call it quits.', 'suspension.jpg'),
(6, 'Car Heating & A/C', 55, 90, 'Keep your car cool and increase your gas mileage with regular AC checks.', 'batteries.jpg'),
(7, 'Steering & Suspension', 45, 120, 'Your car\'s steering and suspension work together to keep your ride smooth and handling precise.', 'car-ac.jpg'),
(8, 'Engine Diagnostic', 350, 240, 'Is your vehicle running bad or stalling? Is your check engine light on? The issue could be as simple as a loose gas cap or as complex as internal engine repair.', 'engine.jpg'),
(9, 'Consultation', 0, 30, 'Stop guessing and start assessing! Know what is going on with your vehicle so you can make an educated decision that will put you and your passengers back on the road safely and with confidence.', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `name`) VALUES
(1, 'pending'),
(2, 'accepted'),
(3, 'declined'),
(4, 'closed');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `hash` varchar(32) NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '2',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expires` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `active`, `hash`, `role_id`, `created_at`, `expires`) VALUES
(4, 'Customer Black', 'customer@domain.com', '$2y$10$I5dWstv1dK1QrJCQZQY/Hu3K5IqiEhRbMixvgZ9m1qmrP1NmfEWOu', 1, '', 2, '2018-06-27 23:34:14', 0),
(5, 'Miss Jane Doe', 'jane2@doe.com', '$2y$10$F3PvFdk8/8oTYUfU4IIjseKxuk09yvXj/VplWq/1gYSJ4qYCL91N2', 0, '', 2, '2018-06-28 16:07:59', 0),
(6, 'Johnny Doe', 'john1454545@doe.com', '$2y$10$B5Zj0AEn/p519BZ.EAeY8.Iwn593r.5N.RhpvTbSh6W.LEbdgZAQa', 1, '', 1, '2018-06-28 16:45:06', 0),
(13, 'Admin Whitey', 'admin@domain.com', '$2y$10$T4lgRD0Ftwoy817pPlUX1.b2Sw8MXjOOFPW7rreucDd1gSPOkvhq.', 1, '', 1, '2018-07-06 02:10:58', NULL),
(14, 'Atila A', 'atila@atila.com', '$2y$10$ksfds4ypw.DxFmMNmjF3z.eo1S0gg00aLr..vY7IP2MB1u8yrUeh2', 0, '7ce3284b743aefde80ffd9aec500e085', 2, '2018-07-06 11:09:07', 1530911347),
(15, 'Atila Atila', 'atila@sdsdsd.abcd', '$2y$10$oRTDy2Z49xP4djOuqf4hje/IctBNQ6pc.ni5LbUhyuq5bYbCYUx6K', 0, '0d3180d672e08b4c5312dcdafdf6ef36', 2, '2018-07-06 11:12:53', 1530911573),
(16, 'Atila A', 'thesino@yahoo.com', '$2y$10$aF/KYkMY9JRq8IRbg5KxNetD4j1/dJiyl30aqcyYsg1EWA107vSPe', 1, '274ad4786c3abca69fa097b85867d9a4', 2, '2018-07-11 16:41:00', 1531363260);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_service`
--
ALTER TABLE `job_service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset`
--
ALTER TABLE `password_reset`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `job_service`
--
ALTER TABLE `job_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `password_reset`
--
ALTER TABLE `password_reset`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
