# References

### PHP PDO Prepared Statements Tutorial to Prevent SQL Injection
https://websitebeaver.com/php-pdo-prepared-statements-to-prevent-sql-injection

### Form validator
http://bootstrapvalidator.votintsev.ru/

### How to Validate (and Sanitize) User Input In PHP Using
https://www.johnmorrisonline.com/validate-sanitize-user-input-php-using-filter_input-filter_var/

### Securely Hash Passwords with PHP (salt included)
https://jonsuh.com/blog/securely-hash-passwords-with-php/

### How to implement email verification
https://code.tutsplus.com/tutorials/how-to-implement-email-verification-for-new-members--net-3824

### How to Create an Email-Based Password Reset Feature For Your Login Script
https://www.johnmorrisonline.com/create-email-based-password-reset-feature-login-script/

### Implementing Secure User Authentication in PHP Applications with Long-Term Persistence
https://paragonie.com/blog/2015/04/secure-authentication-php-with-long-term-persistence

### Force redirect function
https://css-tricks.com/snippets/php/redirect/

### Sending Emails In PHP With PHPMailer
https://github.com/PHPMailer/PHPMailer
https://www.sitepoint.com/sending-emails-php-phpmailer/

### jQuery confirmation
https://craftpip.github.io/jquery-confirm/

### PHP Session-Based Flash Messages
https://mikeeverhart.net/php-flash-messages/

### Recaptcha Implementation
https://www.google.com/recaptcha/
https://stackoverflow.com/questions/45408575/google-recaptcha-validate-using-jquery-ajax

### Bootstrap Multiselect
http://davidstutz.de/bootstrap-multiselect/