<nav class="navbar navbar-expand-lg navbar-dark bg-main" id="navbar">
    <a class="navbar-brand" href="/">
        <img src="/img/logo-customers.png" height="20px" alt="Customers Area">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="/customers/">Dashboard</a>
            </li>
        </ul>
        <!-- Right side -->
        <ul class="navbar-nav ml-auto">
            <a class="nav-link nav-link-login text-warning" href="#">Welcome, <?= $_SESSION['user_fname'] ?></a>
            <a class="btn btn-outline-warning m-1" id="logout-btn" href="/logout.php">Logout</a>
        </ul>
    </div>
</nav>

<header id="header">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1><i class="fas fa-car"></i> Dashboard <small class="text-muted"><?= $pageDetails['tagline']; ?></small></h1>
            </div>
        </div>
    </div>
</header>

<section id="breadcrumb">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Dashboard</li>
                <?php foreach($pageDetails['breadcrumb'] as $folder) : ?>
                <li class="breadcrumb-item"><?= $folder ?></li>
                <?php endforeach; ?>
            </ol>
        </nav>
    </div>
</section>