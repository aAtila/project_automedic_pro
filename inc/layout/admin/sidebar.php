<div class="list-group">
    <a href="/admin/" class="list-group-item list-group-item-action active main-color-bg">
    <i class="fas fa-cog"></i> Dashboard
    </a>
    <a href="/admin/jobs/" class="list-group-item list-group-item-action"><i class="fas fa-wrench"></i> Jobs <span class="badge badge-secondary badge-pill float-right"><?= $countRows[4]->jobs ?></span></a>
    <a href="/admin/users/" class="list-group-item list-group-item-action"><i class="fas fa-user-circle"></i> Users <span class="badge badge-secondary badge-pill float-right"><?= $countRows[1]->users ?></span></a>
    <a href="/admin/cars/" class="list-group-item list-group-item-action"><i class="fas fa-car"></i> Cars <span class="badge badge-secondary badge-pill float-right"><?= $countRows[2]->cars ?></span></a>
    <a href="/admin/reservations/" class="list-group-item list-group-item-action"><i class="fas fa-calendar"></i> Reservations <span class="badge badge-secondary badge-pill float-right"><?= $countRows[4]->jobs ?></span></a>
</div>