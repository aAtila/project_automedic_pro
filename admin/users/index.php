<?php
    $pageDetails = [
        'tagline' => 'manage your users',
        'title' => 'Manage Users'
    ];

    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/header.php');
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/navigation.php');

    $users = getAllUsers($pdo);

?>
    <main>
        <div class="container">
            
            <div class="row">
                <!-- Sidebar -->
                <div class="col-md-3 d-none d-md-block">
                    <?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/sidebar.php'); ?>
                </div>
                <div class="col-md-9">
                <!-- Users overview-->
                <div class="card">
                    <div class="card-header main-color-bg"><i class="far fa-user-circle"></i> Users overview</div>
                        <div class="card-body">
                            <!-- Show flash message -->
                            <?php $msg->display() ?>
                            <table id="customers-table" class="table table-striped table-responsive-md">
                            <thead class="thead-light">
                                <tr>
                                <th scope="col" class="text-center">Name</th>
                                <th scope="col" class="text-center">Email</th>
                                <th scope="col" class="text-center">Status</th>
                                <th scope="col" class="text-center">Role</th>
                                <th scope="col" class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            <?php foreach($users as $user) : ?>
                                <tr>
                                <td class="align-middle text-center"><?= $user->name ?></td>
                                <td class="align-middle text-center"><?= $user->email ?></td>
                                <td class="align-middle text-center"><?= ($user->active === 1) ? '<span class="text-success"><strong>active</strong></span>' : '<span class="text-muted">inactive</span>'; ?></td>
                                <td class="align-middle text-center">
                                    <?php if($user->role == 'admin'): ?>
                                    <span class="text-danger"><strong>admin</strong></span>
                                    <?php else: ?>
                                    <span class="text-warning">customer</span>
                                    <?php endif; ?>
                                </td>
                                <td class="align-middle text-center">
                                    <a href="/admin/users/show/<?= $user->userId ?>" class="btn btn-sm btn-secondary"><i class="far fa-eye"></i></a>
                                    <a href="/admin/users/edit/<?= $user->userId ?>" class="btn btn-sm btn-main"><i class="fas fa-pen"></i></a>
                                    <a href="/admin/users/delete/<?= $user->userId ?>" class="btn btn-sm btn-danger confirm-delete"><i class="far fa-trash-alt"></i></a>
                                </td>
                                </tr>

                            <?php endforeach; ?>

                            </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </main>

<?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/footer.php'); ?>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<script>
    $(document).ready(function() {
    $('#customers-table').DataTable();
} );
</script>