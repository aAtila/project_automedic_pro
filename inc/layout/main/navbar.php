    <nav class="navbar navbar-expand-lg navbar-dark bg-main" id="navbar">
    <a class="navbar-brand" href="/">
        <img src="/img/nav-logo.png" height="20px" alt="">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="/">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#services">Services</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#about">About Us</a>
            </li>
            <li class="nav-item">
                <a class="nav-link price-link" href="#services">Pricing</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#team">Team</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#contact">Contact Us</a>
            </li>
        </ul>
        <!-- Right side -->
        <ul class="navbar-nav ml-auto">
            <?php if(isset($_SESSION['user_role']) && $_SESSION['user_role'] == 1) : ?>
            <a href="/admin/" class="btn btn-outline-warning mx-1" >Admin Area</a>
            <a href="/logout.php" class="btn btn-outline-warning mx-1">Logout</a>
            <?php elseif(isset($_SESSION['user_role']) && $_SESSION['user_role'] == 2) : ?>
            <a href="/customers/" class="btn btn-outline-warning mx-1">Customers Area</a>
            <a href="/logout.php" class="btn btn-outline-warning mx-1">Logout</a>
            <?php else : ?>
            <a href="/login.php" class="btn btn-outline-light mx-1">Login</a>
            <a href="/register.php" class="btn btn-outline-light mx-1">Register</a>
            <?php endif; ?>
        </ul>
    </div>
    </nav>