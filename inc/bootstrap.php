<?php
    // Load config
    require_once $_SERVER["DOCUMENT_ROOT"] . '/inc/config/config.php';
    require_once $_SERVER["DOCUMENT_ROOT"] . '/inc/config/db.php';

    // Load helpers
    require_once $_SERVER["DOCUMENT_ROOT"] . '/inc/helpers/url_helper.php';
    require_once $_SERVER["DOCUMENT_ROOT"] . '/inc/helpers/session_helper.php';
    require_once $_SERVER["DOCUMENT_ROOT"] . '/inc/helpers/db_helper.php';
    require_once $_SERVER["DOCUMENT_ROOT"] . '/inc/helpers/mail_helper.php';
    require_once $_SERVER["DOCUMENT_ROOT"] . '/inc/helpers/debug_helper.php';

    // ChromeLogger allows php output to console
    include $_SERVER["DOCUMENT_ROOT"] . '/ChromePhp.php';
    