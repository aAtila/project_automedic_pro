<?php
    $pageDetails = [
        'tagline' => 'manage your users',
        'title' => 'Edit User'
    ];

    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
    
    if(isset($_GET['id']) && !empty($_GET['id']) && CTYPE_DIGIT($_GET['id'])) {
        $id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);
    } else {
        redirect('/admin/users/');
    }

    // Grab needed data from the db for the form
    $userRoles = getUserRoles($pdo);
    $user = getSingleUser($pdo, $id);

    // Check for POST method
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        // process form
        
        // Sanitize POST data
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        // Init data
        $data = [
            'name' => trim($_POST['name']),
            'email' => trim($_POST['email']),
            'role' => trim($_POST['role']),
            'active' => trim($_POST['active']),
            'name_err' => '',
            'email_err' => ''
        ];

        // Validate email
        if(empty($data['email'])) {
            $data['email_err'] = 'Please enter email';
        } elseif(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $data['email_err'] = 'Not a valid email address';
        } else {
            //Check email
            $user = findUserByEmail($pdo, $data['email']);
            if($user[0] === true && $data['email'] != $user[1]->email) {
                $data['email_err'] = 'Email is already taken';
            }
        }

        // Validate name
        if(empty($data['name'])) {
            $data['name_err'] = 'Please enter name';
        }

        // Make sure errors are empty
        if(empty($data['email_err']) && empty($data['name_err'])) {
            // Update user
            // dd(array($data, $id));
            if(adminUpdateUser($pdo, $data, $id)) {
                $msg->success('User updated.', '/admin/users/');
            } else {
                die('Something went wrong');
            }
        }

    } else {
        // Init data
        $data = [
            'name' => $user->name,
            'email' => $user->email,
            'role' => $user->role_id,
            'active' => $user->active,
            'name_err' => '',
            'email_err' => ''
        ];
    }

    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/header.php');
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/navigation.php');

?>
    <main>
        <div class="container">
            
            <div class="row">
                <!-- Sidebar -->
                <div class="col-md-3 d-none d-md-block">
                    <?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/sidebar.php'); ?>
                </div>
                <div class="col-md-9">
                <!-- Users overview-->
                <div class="card">
                    <div class="card-header main-color-bg"><i class="far fa-edit"></i> Edit user</div>
                        <div class="card-body">
                            <form action="/admin/users/edit/<?= $id ?>" method="post" class="needs-validation" novalidate>
                                <div class="form-group row">
                                    <label for="name" class="col-sm-2 col-form-label">Name:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="name" class="form-control <?= (!empty($data['name_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['name'] ?>" id="name_field">
                                        <span class="invalid-feedback"><?= $data['name_err'] ?></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-2 col-form-label">Email:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="email" class="form-control <?= (!empty($data['email_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['email'] ?>" id="email_field">
                                        <span class="invalid-feedback"><?= $data['email_err'] ?></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="role" class="col-sm-2 col-form-label">Role:</label>
                                    <div class="col-sm-4">
                                        <select name="role" class="form-control" id="role-select">
                                            <?php foreach($userRoles as $role) : ?>
                                                <option value="<?= $role->id ?>" <?php if($role->id == $data['role']): ?> selected="selected"<?php endif; ?>><?= ucwords($role->role) ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="role" class="col-sm-2 col-form-label">Active:</label>
                                    <div class="col-sm-4">
                                        <select name="active" class="form-control" id="active-select">
                                            <option value="1" <?php if($data['active'] == 1): ?> selected="selected"<?php endif; ?>>Active</option>
                                            <option value="0" <?php if($data['active'] == 0): ?> selected="selected"<?php endif; ?>>Inactive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="mt-5">
                                <input type="submit" value="Update User" class="btn main-color-bg" id="register_button">
                                <a href="/admin/users/" class="btn btn-secondary">Back</a>
                                </div>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </main>

<?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/footer.php'); ?>