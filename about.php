<?php
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
    require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/header.php');
?>

    <h1><?= 'title' ?></h1>
    <p><?= 'description' ?></p>

<?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/footer.php'); ?>
