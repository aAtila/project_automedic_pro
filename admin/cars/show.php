<?php
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');

    if(isset($_GET['id']) && !empty($_GET['id']) && CTYPE_DIGIT($_GET['id'])) {

        $pageDetails = [
            'tagline' => 'Car details',
            'title' => 'Car Details'
        ];

        $id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT); // Set user id variable
        $car = getSingleCar($pdo, $id);

        if(!$car) {
            $msg->error('Car does not exist.', '/admin/cars/');
        } else {
            require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/header.php');
            require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/navigation.php');
            ?>

            <main>
                <div class="container">
                    <div class="row">
                        <!-- Sidebar -->
                        <div class="col-md-3 d-none d-md-block">
                            <?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/sidebar.php'); ?>
                        </div>
                        <div class="col-md-9">
                            <!-- Job details-->
                            <div class="card">
                                <div class="card-header main-color-bg"><i class="fas fa-car"></i> Car details</div>
                                    <div class="card-body">
                                        <!-- Show flash message -->
                                        <?php $msg->display() ?>
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <h2>
                                                    <small class="text-muted">plate number</small>
                                                    <strong><?= $car->plate_number ?></strong>
                                                </h2>
                                            </div>
                                            <div>
                                                <a href="/admin/cars/edit/<?= $car->id ?>" class="btn btn-main">Edit Car</a>
                                            </div>
                                        </div>
                                        <!-- Details card -->
                                        <div class="container-fluid bg-light mt-2 pb-3 border">
                                            <div class="mt-3">
                                                <div class="row" style="line-height: 1.8">
                                                    <div class="col-md-3"><strong>Customer:</strong></div>
                                                    <div class="col-md-9"><?= $car->name ?></div>

                                                    <div class="col-md-3"><strong>Brand:</strong></div>
                                                    <div class="col-md-9"><?= $car->brand ?></div>

                                                    <div class="col-md-3"><strong>Model:</strong></div>
                                                    <div class="col-md-9"><?= $car->model ?></div>

                                                    <div class="col-md-3"><strong>Year:</strong></div>
                                                    <div class="col-md-9"><?= $car->year ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- .card-body end -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>

            <?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/footer.php'); ?>

    <?php }} else {
        // redirect('/users/');
        echo 'better luck next time';
    }
?>