<?php

    $pageDetails = [
        'title' => 'Login'
    ];

    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');

    if(isLoggedIn()) {
        redirect('/');
    }

    // Check for POST method
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        // process form

        // Sanitize POST data - gets external variables and optionally filters them
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        // Init data
        $data = [
            'email' => trim($_POST['email']),  // strip whitespace (or other characters) from the beginning and end of a string
            'password' => trim($_POST['password']),
            'email_err' => '',
            'password_err' => ''
        ];

        // Validate email
        if(empty($data['email'])) {
            $data['email_err'] = 'Please enter email';
        }

        // Validate password
        if(empty($data['password'])) {
            $data['password_err'] = 'Please enter password';
        }

        // Check for user/email
        $user = findUserByEmail($pdo, $data['email']);

        if($user[0] === true) {
            if($user[1]->active === 0) {
                $data['email_err'] = ' ';
                $msg->error('You need to verify your account to be able to use our services. Please click on the verification link that was sent to your email address, or request a new one.');
            }
        } else {
            $data['email_err'] = 'No user found';
        }

        // Make sure errors are empty
        if(empty($data['email_err']) && empty($data['name_err']) && $user[1]->active === 1) {
            // Validated
            // Check and set logged in user
            $loggedInUser = loginUser($pdo, $data['email'], $data['password']);

            if($loggedInUser) {
                // Create session
                createUserSession($loggedInUser);
            } else {
                $data['password_err'] = 'Password incorrect';
            }
        }

    } else {
        // Init data
        $data = [
            'email' => '',
            'password' => '',
            'email_err' => '',
            'password_err' => ''
        ];
    }

    require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/header.php');

?>
    <div class="container">
    <div class="row mt-5">
        <div class="col-md-6 mx-auto">
            <div class="card card-body bg-light mt-5 mb-5">
            <?= $msg->display() ?>
                <h2>Login</h2>
                <p>Please fill in your credentials to log in.</p>
                <form action="<?= htmlspecialchars(CURRPATH) ?>" method="post" class="needs-validation" novalidate>
                    <div class="form-group">
                        <label for="email">Email: <sup>*</sup></label>
                        <input type="text" name="email" class="form-control <?= (!empty($data['email_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['email'] ?>">
                        <span class="invalid-feedback"><?= $data['email_err'] ?></span>
                    </div>
                    <div class="form-group">
                        <label for="password">Password: <sup>*</sup></label>
                        <input type="password" name="password" class="form-control <?= (!empty($data['password_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['password'] ?>">
                        <span class="invalid-feedback"><?= $data['password_err'] ?></span>
                    </div>

                    <div class="row mt-4">
                        <div class="col">
                            <input type="submit" value="Login" class="btn btn-block main-color-bg">
                        </div>
                        <div class="col">
                            <a href="/reset_password.php" class="btn btn-light btn-block">
                            Lost password?</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/footer.php'); ?>