<?php
require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');

if(!isLoggedIn()) {
    $msg->error('You need to be logged in to access this page.', '/login.php');
} elseif(isUser()) {
    $msg->error('You don\'t have permissions to access that page.', '/customers/');
}

//delete.php
if(isset($_POST["id"]))
{
    //$connect = new PDO('mysql:host=localhost;dbname=automedic', 'root', '123456');
    $query = "
        DELETE from jobs WHERE id=:id ";
    $statement = $connect->prepare($query);
    $statement->execute(
        array(
            ':id' => $_POST['id']
        )
    );
}
?>