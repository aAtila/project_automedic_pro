$(document).ready(function() {

    var currentDate = $('#calendar').fullCalendar('getDate');
    
    var calendar = $('#calendar').fullCalendar({
        header:{
            left:'prev, next today',
            center:'title',
            right:'month, agendaWeek, agendaDay'
        },
        // disable scrolling
        // prevent resize of the duration of event           
        eventDurationEditable: true,
        eventStartEditable: true,
        eventResourceEditable: true,
        allDayDefault: false,
        aspectRatio: 2,
        height: "auto",             
        navLinks: true, // can click day/week names to navigate views
        // set the master editable property to false, and current to true
        editable: true, // prevent drag and drop
        eventLimit: true, // allow "more" link when too many events
        // select multiple days or hours
        selectable: true,
        selectHelper: true,
        minTime: "7:00",
        maxTime: "19:00",
        allDaySlot: false,
        // don't allow to overlap
        selectOverlap: false,
        // draging no overlap
        eventOverlap: false,   
        slotDuration: "00:30:00",
        // load event        
        events: 'load.php',
        eventColor: 'orange',     
        //displayEventTime: false,  
        weekends: false,

        // this function limits the range for dates
        validRange: function(nowDate) {            
            return {
            start: currentDate,
            end: nowDate.clone().add(6, 'months')
            };
        },
        
        // color and hover
        dayRender: function(date, cell) {
            cell.append("<span class='hoverEffect' style='display:none;'></span>");
            cell.mouseenter(function() {
                cell.find(".hoverEffect").show();
                cell.css("background", "rgba(0,255,0,.3)");
            }).mouseleave(function() {
                $(".hoverEffect").hide();
                cell.removeAttr('style');
            });
        },     

        dayClick: function(date, event, view) {
            if(view.name == 'month') {                
                $('#calendar').fullCalendar('changeView', 'agendaWeek');
                $('#calendar').fullCalendar('gotoDate', date);
            }
        },

        eventDrop: function(event) {
            var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
            var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss"); 
            var id = event.id;  
            swal({
                title: "You are to reschedule this appointment!",
                text: "Happy with appointment starting on " + start + " and finishing on " + end, 
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })                     
            .then((dosomething) => {
                if (dosomething) {
                  $.ajax({
                      url: "update.php",
                      type: "POST",
                      data: {                    
                        //cars_id: myId,
                        id: id,                    
                        start: start,
                        end: end                    
                    },                      
                      success: function() {
                          calendar.fullCalendar('refetchEvents');
                          //alert("Added Successfully");                                                     
                          swal("You rescheduled the appointment!", {
                              icon: "success"                        
                          })                          
                      }                    
                  });
                } else {
                    $('#calendar').fullCalendar('refetchEvents');                      
              }
            });
        },

        eventResize: function(event, delta, revertFunc) {
            var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
            var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
            var id = event.id; 
            swal({
                title: "You are to reschedule this appointment!",
                text: "Happy with appointment starting on " + start + " and finishing on " + end, 
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((dosomething) => {
                if (dosomething) {
                  $.ajax({
                      url: "update.php",
                      type: "POST",
                      data: {                    
                        //cars_id: myId,
                        id: id,                    
                        start: start,
                        end: end                    
                    },                      
                      success: function() {
                          calendar.fullCalendar('refetchEvents');
                          //alert("Added Successfully");                                                     
                          swal("You rescheduled the appointment!", {
                              icon: "success"                        
                          })                          
                      }                    
                  });
                } else {
                    $('#calendar').fullCalendar('refetchEvents');                      
              }
            });    
        },

        eventClick: function(event) {
            var id = event.id;
            var start = "0000-00-00 00:00:00";                    
            var end = "0000-00-00 00:00:00";
            swal({
                title: "You are to remove this appointment!",
                text: "Are you sure?", 
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((dosomething) => {
                if (dosomething) {
                  $.ajax({
                      url: "update.php",
                      type: "POST",
                      data: {                    
                        //cars_id: myId,
                        id: id,                    
                        start: start,
                        end: end                    
                    },                      
                      success: function() {
                          calendar.fullCalendar('refetchEvents');
                          //alert("Added Successfully");                                                     
                          swal("You removed an appointment!", {
                              icon: "success"                        
                          })                          
                      }                    
                  });                      
                } else {
                      $('#calendar').fullCalendar('unselect');                      
                }
              });
            },

            select: function(start, end, date, view) {
                $('#calendar').fullCalendar('unselect');
            } 
    });
});

// this in console
//jQuery("#calendar").fullCalendar("clientEvents")
