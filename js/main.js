// Form Validator

// Wait for the DOM to be ready
$(document).ready(function() {
    $('.needs-validation').bootstrapValidator({
        message: 'This value is not valid',
        excluded: [':disabled', ':hidden', ':not(:visible)'],
        feedbackIcons: {
            valid: '',
            invalid: '',
            validating: ''
        },
        fields: {
            name: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Name is required and cannot be empty'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'The name must be at least 6 characters long'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Email is required and cannot be empty'
                    },
                    regexp: {
                        regexp: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                        message: 'The input is not a valid email address'
                    }
                }
            },
            password: {
                message: 'The passowrd is not valid',
                validators: {
                    notEmpty: {
                        message: 'Password is required and cannot be empty'
                    },
                    stringLength: {
                        min: 6,
                        message: 'The password must be at least 6 characters long'
                    }
                }
            },
            confirm_password: {
                message: 'The passowrd is not valid',
                validators: {
                    notEmpty: {
                        message: 'Confirm password is required and cannot be empty'
                    },
                    stringLength: {
                        min: 6,
                        message: 'The password must be at least 6 characters long'
                    }
                }
            },
            role: {
                validators: {
                    notEmpty: {
                        message: 'You need to choose a role'
                    }
                }
            },
            car_id: {
                validators: {
                    notEmpty: {
                        message: 'You need to choose a car'
                    }
                }
            },
            'services[]': {
                validators: {
                    notEmpty: {
                        message: 'You need to select at least one service'
                    }
                }
            },
            plate_number: {
                validators: {
                    notEmpty: {
                        message: 'Plate number is required and cannot be empty'
                    },
                    stringLength: {
                        min: 2,
                        message: 'The plate number must be at least 2 characters long'
                    }
                }
            },
            plate_number_field: {
                validators: {
                    notEmpty: {
                        message: 'Plate number is required and cannot be empty'
                    },
                    stringLength: {
                        min: 2,
                        message: 'The plate number must be at least 2 characters long'
                    }
                }
            },
            brand: {
                validators: {
                    notEmpty: {
                        message: 'Brand is required and cannot be empty'
                    }
                }
            },
            model: {
                validators: {
                    notEmpty: {
                        message: 'Model is required and cannot be empty'
                    }
                }
            },
            year: {
                validators: {
                    notEmpty: {
                        message: 'Year is required and cannot be empty'
                    },
                    stringLength: {
                        min: 4,
                        max: 4,
                        message: 'The year must be 4 characters long '
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: 'Only numbers'
                    }
                }
            }
        }
    })
    .on('error.validator.bv', function(e, data) {
        // $(e.target)    --> The field element
        // data.bv        --> The BootstrapValidator instance
        // data.field     --> The field name
        // data.element   --> The field element
        // data.validator --> The current validator name

        data.element
            .data('bv.messages')
            // Hide all the messages
            .find('.help-block[data-bv-for="' + data.field + '"]').hide()
            // Show only message associated with current validator
            .filter('[data-bv-validator="' + data.validator + '"]').show();
    });
});

// Confirm delete entry
$('a.confirm-delete').confirm({
    icon: 'fas fa-minus-circle',
    theme: 'supervan',
    title: 'Delete',
    content: 'Are you sure you want to delete this and all associated entries?',
    type: 'red',
    typeAnimated: true,
    buttons: {
        delete: {
            text: 'Delete',
            btnClass: 'btn-red',
            action: function(){
                location.href = this.$target.attr('href');
            }
        },
        cancel: {
            text: 'Cancel',
            action: function(){
            }
        }
    }
});