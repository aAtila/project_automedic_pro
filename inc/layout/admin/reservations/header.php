<?php
    if(!isLoggedIn()) {
        $msg->error('You need to be logged in to access this page.', '/login.php');
    } elseif(isUser()) {
        $msg->error('You don\'t have permissions to access that page.', '/customers/');
    }

    $countRows = countRows($pdo);

    $url = explode('/', $_SERVER['REQUEST_URI']);
    // Iterate through the array
    $breadcrumb = array_filter( $url, function($entry) {
        if(!empty($entry) && ctype_alpha($entry)) {
            return $entry;
        };
    });
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/css/animate.css">
    <link rel="stylesheet" href="/css/fullcalendar.css">
    <link rel="stylesheet" href="/css/style.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/moment.min.js"></script>
    <script src="/js/fullcalendar.js"></script>
    <script src="/js/reservations_cal_admin.js"></script>
    <title><?= SITENAME.' | '.$pageDetails['title'] ?></title>
</head>
<body>