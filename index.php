<?php
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
    $services = getAllServices($pdo);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Automedic | Best Local Auto Repair Service</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="auto repair, auto service, car repair, auto care" name="keywords">
    <meta content="From factory recommended maintenance to complete auto repair, our auto service experts can help keep your car on the road longer." name="description">

    <!-- Facebook Opengraph integration: https://developers.facebook.com/docs/sharing/opengraph -->
    <meta property="og:title" content="Best Local Auto Repair Service">
    <meta property="og:image" content="">
    <meta property="og:url" content="https://automedic.icu">
    <meta property="og:site_name" content="AutoMedic">
    <meta property="og:description" content="From factory recommended maintenance to complete auto repair, our auto service experts can help keep your car on the road longer.">

    <!-- Twitter Cards integration: https://dev.twitter.com/cards/  -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="https://automedic.icu">
    <meta name="twitter:title" content="Best Local Auto Repair Service">
    <meta name="twitter:description" content="From factory recommended maintenance to complete auto repair, our auto service experts can help keep your car on the road longer.">
    <meta name="twitter:image" content="">

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" href="/css/animate.css">
    <link rel="stylesheet" href="/css/style.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>

    <!-- Hero -->
    <section class="hero">
    <div class="container text-center">
        <div class="row">
        <div class="col-md-12">
            <a class="hero-brand" href="/index.php" title="Home"><img alt="Bell Logo" src="img/logo.png"></a>
        </div>
        </div>

        <div class="col-md-12">
        <h1>
            Adding more life to your car
            </h1>
        <p class="tagline">
            From factory recommended maintenance to complete auto repair, our auto service experts can help keep your car on the road longer.
        </p>
        <a class="btn btn-hero btn-lg" href="tel:+1-800-2886-63342">CALL NOW: <strong>0800-AUTO-MEDIC</strong></a>
        </div>
    </div>
    </section>
    <!-- /Hero -->

    <!-- Navigation -->
    <?php require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/navbar.php'); ?>
    <!-- /Navigation -->
    
    <!-- Services -->
    <section class="services" id="services">
    <div class="container">
        <h2 class="text-center">
            Complete Auto Care Program
        </h2>
        <p class="text-center mb-3">Maintaining your vehicle's health is always in your best interest. Cars that are well taken care of outperform and outlast those that are not. When something feels “off” with your vehicle, don't hesitate to bring it to AutoMedic's Auto Care Service.</p>
        <div class="row pt-lg-3">
        <?php foreach($services as $service) : ?>
        <div class="col-lg-3 col-sm-6 col-xs-12 my-3">
            <div class="card card-home card-block text-center card-service">
                <img class="card-img-top" src="img/<?=$service->image?>" alt="<?=$service->name?> Services">
                <div class="card-body bg-main">
                <h5 class="text-light"><?=$service->name?></h5>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
        </div>
    </div>
    </section>
    <!-- /Services -->

    <!-- About -->
    <section class="about-us" id="about">
        <div class="row align-items-center darken" style="max-width: 100%">
        <div class="col-lg-6 bg-dark intro">
        <h2>About Our Company</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laborisr sit amet, consectetur adipisicing magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laborisr sit amet, consectetur   quis nostrud exercitation ullamco laborisr sit amet, consectetur adipisicing magna aliqua.</p>
        <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation  veniam, quis ud exercitation.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laborisr sit amet, consectetur adipisicing magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laborisr sit amet, consectetur   quis nostrud exercitation ullamco laborisr sit amet, consectetur adipisicing magna aliqua.</p>
        </div>
        <div class="col-lg-6">
        </div>
    </div>
    </section>
    <!-- About -->

    <!-- Benefits -->
    <section class="services" id="services">
        <div class="container">
        <h2 class="text-center">Benefits of Working With Us</h2>
        <p class="text-center">Our Auto Care experience is a hassle-free experience. Your car will be serviced right the first time — guaranteed.</p>
        <div class="row pt-lg-3">

            <div class="col-lg-4 col-xs-12 my-3">
                <div class="card card-home card-block text-center card-service">
                <div class="card-body m-1">
                    <i class="far fa-user-circle display-4 my-3 text-main"></i>
                    <h3 class="text-main">
                    Fully Trained Mechanics</h3>
                    <p class="card-text">Rem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod ut labore et dolore magna.</p>
                    <a href="#" class="card-link"></a>
                </div>
                </div>
            </div>
            <div class="col-lg-4 col-xs-12 my-3">
                <div class="card card-home card-block text-center card-service">
                <div class="card-body m-1">
                    <i class="fas fa-clipboard-list display-4 my-3 text-main"></i>
                    <h3 class="text-main">
                        Car Service Bulletins</h3>
                    <p class="card-text">Mipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tet dolore magna.</p>
                    <a href="#" class="card-link"></a>
                </div>
                </div>
            </div>
            <div class="col-lg-4 col-xs-12 my-3">
                <div class="card card-home card-block text-center card-service">
                <div class="card-body m-1">
                    <i class="fas fa-wrench display-4 my-3 text-main"></i>
                    <h3 class="text-main">
                        New Car Warranty</h3>
                    <p class="card-text">Em ipsum dolor sit amet, consectetur adipisicing elit, sed dodidunt ut labore et dolore magna.</p>
                    <a href="#" class="card-link"></a>
                </div>
                </div>
            </div>
            
        </div>
        </div>
    </section>
    <!-- /Benefits -->

    <!-- Problems -->
    <section class="problem">
        <div class="row align-items-center darken" style="max-width: 100%">
        <div class="col-lg-6">
        </div>
        <div class="col-lg-6 bg-dark intro">
        <h2>We Solve Problems</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laborisr sit amet, consectetur adipisicing magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laborisr sit amet, consectetur   quis nostrud exercitation ullamco laborisr sit amet, consectetur adipisicing magna aliqua.</p>
        <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation  veniam, quis ud exercitation.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laborisr sit amet, consectetur adipisicing magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laborisr sit amet, consectetur   quis nostrud exercitation ullamco laborisr sit amet, consectetur adipisicing magna aliqua.</p>
        <a class="btn btn-hero btn-ghost" href="#contact">Get Your FREE Consultation</a>
        </div>
    </div>
    </section>
    <!-- /Problems -->

    <!-- Team -->
    <section class="team" id="team">
        <div class="container text-center">
            <h2>
                Meet The Team
            </h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam feugiat viverra lectus eu commodo. Aliquam at sapien sollicitudin metus convallis molestie. Donec molestie ultrices fermentum. Nullam in odio est. Donec mollis dui vel enim gravida rutrum. Morbi egestas est non massa posuere finibus.</p>
            <div class="row pt-3">
                <div class="col-md-6">
                    <p>Vivamus in ligula in est euismod placerat. Donec quis iaculis diam. Nam a dolor porta, aliquam tellus in, auctor massa. Nullam consectetur rhoncus felis eu dictum. Quisque nec sapien tristique, laoreet tortor placerat, malesuada eros. Suspendisse pretium nisl erat, nec consequat libero ultricies id.</p>
                    <p>Nunc a fringilla orci. Pellentesque quam turpis, maximus ornare urna nec, semper porttitor quam. Sed vehicula rhoncus tellus, in tristique lorem iaculis ut.</p>
                </div>
                <div class="col-md-6">
                    <p>Suspendisse lacinia lorem quis magna ultrices lacinia. Mauris consectetur risus at luctus tempus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                    <p>Praesent tristique nisl id purus facilisis pellentesque. Sed consequat eleifend urna vitae laoreet. Nam vestibulum hendrerit mi ac porttitor. Morbi ornare sapien rutrum ante congue, a tincidunt lacus convallis. Nunc fermentum ex felis, eget ornare elit congue ut.</p>
                </div>
            </div>
        </div>

        <div class="team-grid">
        <div class="row">
            <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="card card-home card-block">
                <img alt="" src="img/team-1.jpg">
                <div class="team-over">
                    <div>
                    <h3 class="card-title">Virgil Shannon</h3>
                    <p class="card-text">Detail Technician</p>
                    </div>
                </div>
            </div>
            </div>

            <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="card card-home card-block">
                <img alt="" src="img/team-4.jpg">
                <div class="team-over">
                    <div>
                    <h3 class="card-title">Lucas Dawson</h3>
                    <p class="card-text">Master Technician</p>
                    </div>
                </div>
            </div>
            </div>

            <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="card card-home card-block">
                <img alt="" src="img/team-3.jpg">
                <div class="team-over">
                    <div>
                    <h3 class="card-title">Kennard Kelvin</h3>
                    <p class="card-text">The Tire & Wheels Guy</p>
                    </div>
                </div>
            </div>
            </div>

            <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="card card-home card-block">
                <img alt="" src="img/team-8.jpg">
                <div class="team-over">
                    <div>
                    <h3 class="card-title">Justin Kyle</h3>
                    <p class="card-text">The Boss</p>
                    </div>
                </div>
            </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="card card-home card-block">
                <img alt="" src="img/team-5.jpg">
                <div class="team-over">
                    <div>
                    <h3 class="card-title">Daniella Moira</h3>
                    <p class="card-text">Service Manager</p>
                    </div>
                </div>
            </div>
            </div>

            <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="card card-home card-block">
                <img alt="" src="img/team-6.jpg">
                <div class="team-over">
                    <div>
                    <h3 class="card-title">Sonny Hilary</h3>
                    <p class="card-text">Race & Luxury Car Specialist</p>
                    </div>
                </div>
            </div>
            </div>

            <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="card card-home card-block">
                <img alt="" src="img/team-7.jpg">
                <div class="team-over">
                    <div>
                    <h3 class="card-title">Devon Kasey</h3>
                    <p class="card-text">Tecnician</p>
                    </div>
                </div>
            </div>
            </div>

            <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="card card-home card-block">
                <img alt="" src="img/team-9.jpg">
                <div class="team-over">
                    <div>
                    <h3 class="card-title">Black Jack</h3>
                    <p class="card-text">The Tool Keeper</p>
                    </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </section>
    <!-- /Team -->

    <!-- Testimonials -->
    <section class="testimonials" id="testimonials">
        <div class="container">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Carousel indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>   
                <!-- Wrapper for carousel items -->
                <div class="carousel-inner">
                    <div class="item carousel-item active">
                        <div class="img-box"><img src="/img/customer-1.jpg" alt=""></div>
                        <p class="testimonial">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>
                        <p class="overview"><b>Paula Wilson</b>, Landscape Architect</p>
                    </div>
                    <div class="item carousel-item">
                        <div class="img-box"><img src="/img/customer-2.jpg" alt=""></div>
                        <p class="testimonial">Vestibulum quis quam ut magna consequat faucibus. Pellentesque eget nisi a mi suscipit tincidunt. Utmtc tempus dictum risus. Pellentesque viverra sagittis quam at mattis. Suspendisse potenti. Aliquam sit amet gravida nibh, facilisis gravida odio.</p>
                        <p class="overview"><b>Antonio Moreno</b>, Market Research Analyst</p>
                    </div>
                    <div class="item carousel-item">
                        <div class="img-box"><img src="/img/customer-3.jpg" alt=""></div>
                        <p class="testimonial">Phasellus vitae suscipit justo. Mauris pharetra feugiat ante id lacinia. Etiam faucibus mauris id tempor egestas. Duis luctus turpis at accumsan tincidunt. Phasellus risus risus, volutpat vel tellus ac, tincidunt fringilla massa. Etiam hendrerit dolor eget rutrum.</p>
                        <p class="overview"><b>Michael Holz</b>, School Counselor</p>
                    </div>
                </div>
                <!-- Carousel controls -->
                <a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>

        </div>
    </section>
    <!-- /Testimonials -->

    <!-- Call to Action -->
    <section class="cta">
        <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-9 col-sm-12 text-lg-left intro">
            <h2>Because So Much Is Riding On Your Tires</h2>
            <p>We don’t just service cars, <strong>we service people’s cars!</strong> Let us take care of you and your car.</p>
            <p>At AutoMedic we understand the trouble of losing your mobility (your car) even for just a few moments. We also understand that you need smooth, fast, reliable service and the impact if that is not achieved. It’s not just a car, it’s an integral part of your everyday life.</p>
            </div>

            <div class="col-lg-3 col-sm-12 text-lg-right text-center">
            <a class="btn btn-hero btn-ghost" href="#">Make an Appointment</a>
            </div>
        </div>
        </div>
    </section>
    <!-- /Call to Action -->

    <!-- Footer -->
    <section id="contact">
        <div class="container">
        <div class="row justify-content-center">
        <h2>Contact Us</h2>
            <div class="col-md-12 text-center">
                <div class="row justify-content-center">
                    <div class="col-lg-3 col-md-4 pb-5">
                        <div class="row info p-3">
                            <div class="col-3"><i class="fa fa-map-marker"></i></div>
                            <div class="col-9 pt-1">4321 Washington Road<br>Evans, GA 30809</div>
                        </div>
                        <div class="row info p-3">
                            <div class="col-3"><i class="fa fa-envelope"></i></div>
                            <div class="col-9 pt-1">hello@automedic.icu</div>
                        </div>
                        <div class="row info p-3">
                            <div class="col-3"><i class="fa fa-phone"></i></div>
                            <div class="col-9 pt-1">+1 0800 AUTO MEDIC</div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-8">
                        <div id="send-message"></div>
                        <form action="" method="post" role="form" id="contact-form" class="needs-validation" novalidate>
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" id="contact-name" placeholder="Your Name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="email" id="contact-email" placeholder="Your Email">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="subject" id="contact-subject" placeholder="Subject">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="message" rows="5" id="contact-message" placeholder="Message"></textarea>
                        </div>
                        <input type="submit" value="Send Message" class="btn btn-block main-color-bg" id="send-button">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    <!-- /Footer -->

    <!-- Maps -->
    <section id="map"></section>
    <!-- /Maps -->

    <footer class="site-footer">
        <div class="bottom">
        <div class="container">
            <div class="row">

            <div class="col-lg-6 col-xs-12 text-lg-left text-center">
                <p class="text-muted">
                <small>&copy; <?php echo date("Y"); ?> AutoMedic · Handcrafted with ❤️ and lot of 💦 in sunny Subotica 🌞</small>
                </p>
                <div class="credits">
                </div>
            </div>

            <div class="col-lg-6 col-xs-12 text-lg-right text-center">
                <ul class="list-inline">
                <li class="list-inline-item">
                    <a href="/">Home</a>
                </li>

                <li class="list-inline-item">
                    <a href="#services">Services</a>
                </li>

                <li class="list-inline-item">
                    <a href="#about">About Us</a>
                </li>

                <li class="list-inline-item">
                    <a href="#services" class="price-link">Prices</a>
                </li>

                <li class="list-inline-item">
                    <a href="#team">Team</a>
                </li>

                <li class="list-inline-item">
                    <a href="#contact">Contact</a>
                </li>
                </ul>
            </div>

            </div>
        </div>
        </div>
    </footer>
    <a class="scrolltop" href="#"><span class="fa fa-angle-up"></span></a>

    <!-- Pricing Modal -->
    <div class="modal fade modal-price" id="price-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-body">
            <div>
                <button type="button" class="btn btn-sm btn-secondary mb-3 float-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <table class="table table-striped table-dark m-0 rounded shadow">
            <thead>
                <tr>
                <th scope="col">Service</th>
                <th scope="col">Description</th>
                <th scope="col">Price</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($services as $service) : ?>
                <tr>
                <td class="align-items-start"><?=$service->name?></td>
                <td class="text-muted"><?php echo htmlentities($service->description); ?></td>
                <td class="align-middle"><strong>$<?=$service->price?></strong></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
            </table>
            <div class="row mt-4">
                <div class="col"><a href="#contact" class="btn btn-block btn-lg btn-main">GET YOUR FREE CONSULTATION</a></div>
                <div class="col"><a href="tel:+1-800-2886-63342" class="btn btn-block btn-lg btn-main">+1-800-AUTO-MEDIC</a></div>
            </div>
        </div>
        </div>
    </div>
    </div>
    <!-- Pricing Modal -->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script src="/js/main.js"></script>
<script src="/js/homepage.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA0s1a7phLN0iaD6-UE7m4qP-z21pH0eSc&callback=initMap"></script>
</body>
</html>

<script>
    $(document).ready(function(){
    $('#contact-form').submit(function(event){
        $('#send-button')[0].value = 'Sending...';
        var formData = {
        'name'  : $('#contact-name').val(),
        'email'  : $('#contact-email').val(),
        'subject'  : $('#contact-subject').val(),
        'message'  : $('#contact-message').val()
        };
        $.post('/contact_us.php', formData, function(data) {
           // Append data into #send-message div
           $('#send-message').html(data);
           $('#send-button')[0].value = 'Send Message';
           $('#contact-form')[0].reset();
       });
       event.preventDefault();
    });

    // Show Modal
   $('.price-link, .card-service').click(function() {
       $('#price-modal').modal('show');
   });

})
</script>