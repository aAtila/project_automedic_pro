<?php
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
    
    unset($_SESSION['user_id']);
    unset($_SESSION['user_email']);
    unset($_SESSION['user_name']);
    session_destroy();
    redirect('/');