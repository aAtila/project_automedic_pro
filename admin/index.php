<?php
    $pageDetails = [
        'tagline' => 'manage your shop',
        'title' => 'Manage Shop'
    ];
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
    include ($_SERVER["DOCUMENT_ROOT"] . '/admin/pagination.php');

    $pendingJobs = getPendingJobs($pdo, $paginationData);
    $num = $offset + 1;
    $countRows = countRows($pdo);

    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/header.php');
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/navigation.php');
?>

    <main>
        <div class="container">
            <div class="row">
                <!-- Sidebar -->
                <div class="col-md-3 d-none d-md-block">
                    <?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/sidebar.php'); ?>
                </div>
                <div class="col-md-9">
                <!-- Shop Overview -->
                <div class="card mb-4">
                    <div class="card-header main-color-bg"><i class="fas fa-store-alt"></i> Shop Overview</div>
                        <div class="card-body">
                            <div class="card-deck d-flex">
                                <!-- Pending Jobs Warning -->
                                <?php if($pendingJobs !== false): ?>
                                <div class="p-2 flex-fill">
                                    <div class="card bg-light text-main text-center animated flash">
                                        <div class="card-body">
                                            <h2><i class="fas fa-hourglass-half"></i> <strong><?=$pendingJobs[1]?></strong></h2>
                                            <h5><strong>Pending Submissions</strong></h5>
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>
                                <div class="p-2 flex-fill">
                                    <div class="card bg-light text-center">
                                        <div class="card-body">
                                            <h2><i class="fas fa-wrench"></i> <strong><?= $countRows[3]->openJobs ?></strong></h2>
                                            <h5><strong>Open Jobs</strong></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="p-2 flex-fill">
                                    <div class="card bg-light text-center">
                                        <div class="card-body">
                                            <h2><i class="fas fa-id-card-alt"></i> <strong><?= $countRows[0]->customers ?></strong></h2>
                                            <h5><strong>Customers</strong></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="p-2 flex-fill">
                                    <div class="card bg-light text-center">
                                        <div class="card-body">
                                            <h2><i class="fas fa-car"></i> <strong><?= $countRows[2]->cars ?></strong></h2>
                                            <h5><strong>Cars</strong></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <!-- Pending Submissions -->
                <div class="card">
                    <div class="card-header main-color-bg"><i class="fas fa-hourglass-half"></i> Pending Submissions</div>
                        <div class="card-body">
                        <?php if($pendingJobs !== false): ?>
                            <table class="table table-hover table-striped table-responsive-md">
                            <thead class="thead-light">
                                <tr>
                                <th scope="col" class="text-center">#</th>
                                <th scope="col" class="text-center">Customer</th>
                                <th scope="col" class="text-center">Car</th>
                                <th scope="col" class="text-center">Date Submitted</th>
                                <th scope="col" class="text-center">Approve / Disapprove</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($pendingJobs[0] as $job) : ?>
                                <tr class="table-row" data-href="/admin/jobs/edit/<?= $job->JobId ?>">
                                <td class="align-middle text-center"><strong><?= $num++ ?><strong></td>
                                <td class="align-middle text-center"><?= $job->userName ?></td>
                                <td class="align-middle text-center">
                                    <button type="button" class="btn btn-sm btn-outline-primary mt-1" disabled id="plate-btn-<?= $job->JobId ?>"><?= $job->plateNumber ?></button>
                                </td>
                                <td class="align-middle text-center"><?= $job->dateCreated ?></td>
                                <td class="align-middle text-center">
                                    <a href="/admin/jobs/edit/<?= $job->JobId ?>"><button type="button" class="btn btn-sm mt-1 btn-main"><i class="fas fa-pen"></i> Moderate</button></a>
                                </td>
                                </tr>

                            <?php endforeach; ?>
                            </tbody>
                            </table>
                            <!-- Pagination -->
                            <nav aria-label="Page navigation" class="float-right">
                            <ul class="pagination">
                                <li class="page-item <?php if($page <= 1){ echo 'disabled'; } ?>">
                                    <a class="page-link" href="<?php if($page <= 1){ echo '#'; } else { echo "?page=".($page - 1); } ?>">Prev</a>
                                </li>
                                <?php for ($i=1; $i < $pendingJobs[2] + 1; $i++) {
                                    if($page == $i) {
                                        echo '<li class="page-item active">';
                                    }else {
                                        echo '<li class="page-item">';
                                    }
                                echo '<a class="page-link" href="?page='.$i.'">'.$i.'</a></li>';
                                }
                                ?>
                                <li class="page-item <?php if($page >= $pendingJobs[2]){ echo 'disabled'; } ?>">
                                    <a class="page-link" href="<?php if($page >= $pendingJobs[2]){ echo '#'; } else { echo "?page=".($page + 1); } ?>">Next</a>
                                </li>
                            </ul>
                            </nav>
                            <?php else: ?>
                                <p class="text-center">No pending jobs.</p>
                            <?php endif; ?>
                        </div>
                </div>
            </div>
        </div>
    </main>

<?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/footer.php'); ?>

<!-- Make table row clickable -->
<script type="text/javascript">
$(document).ready(function($) {
    $(".table-row").click(function() {
        window.document.location = $(this).data("href");
    });
});
</script>