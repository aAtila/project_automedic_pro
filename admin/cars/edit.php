<?php
    $pageDetails = [
        'tagline' => 'manage your cars',
        'title' => 'Edit User'
    ];

    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/header.php');
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/navigation.php');
    
    if(isset($_GET['id']) && !empty($_GET['id']) && CTYPE_DIGIT($_GET['id'])) {
        $id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);
        //var_dump($id);        
    } else {
        redirect('/admin/cars/');
    }

    // Grab needed data from the db for the form
    //$userRoles = getUserRoles($pdo);
    $car = getSingleCar($pdo, $id);
    //echo 'car<br>';
    //var_dump($car);
    //echo '<br>';

    // Check for POST method
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        // process form
        
        // Sanitize POST data
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        // Init data
        $data = [
            'name' => trim($_POST['name']),
            'plate_number' => trim($_POST['plate_number']),
            'brand' => trim($_POST['brand']),
            'model' => trim($_POST['model']),
            'year' => trim($_POST['year']),
            'name_err' => '',
            'plate_number_err'=> '',
            'brand_err' => '',
            'model_err' => '',
            'year_err' => ''
        ];
        //var_dump($data);
        // validate plate number        
        if(empty($data['plate_number'])) {
            $data['plate_number_error'] = 'Please enter plate number'; // add more checks
        } else {
            // check if plate number already in dbase
            $car = checkDuplicatePlateNumber($pdo, $data['plate_number']);
            //var_dump('carcar', $car);
            if($car[0] === true && $id != $car[1]->id) {
                //$data['plate_number_err'] = 'That plate number already exists in database';
                $msg->warning('Plate number already in database.', '/admin/cars/');
            }
            //if(checkDuplicatePlateNumber($pdo, $data['plate_number'])) {
            //    $msg->warning('Plate number already in database', '/admin/cars/');
             //   //$data['plate_number_err'] = 'That plate number already exists in database';
           // }
        }

        // Validate name and rest of it
        if(empty($data['name'])) {
            $data['name_err'] = 'Please enter name';
        }

        // Make sure errors are empty
        if(empty($data['plate_number_err']) && empty($data['name_err'])) {
            // Update car
            // dd(array($data, $id));
            if(adminUpdateCar($pdo, $data, $id)) {
                $msg->success('Car updated.', '/admin/cars/');
            } else {
                die('Something went wrong');
            }
        }

    } else {
        // Init data
        $data = [
            'name' => $car->name,
            'plate_number' => $car->plate_number,
            'brand' => $car->brand,
            'model' => $car->model,
            'year' => $car->year,
            'name_err' => '',
            'plate_number_err'=> '',
            'brand_err' => '',
            'model_err' => '',
            'year_err' => ''
        ];
    }    
    //var_dump($data);
?>
    <main>
        <div class="container">
            
            <div class="row">
                <!-- Sidebar -->
                <div class="col-md-3">
                    <?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/sidebar.php'); ?>
                </div>
                <div class="col-md-9">
                <!-- Cars overview-->
                <div class="card">
                    <div class="card-header main-color-bg"><i class="far fa-edit"></i> Car editing</div>
                        <div class="card-body">
                            <form action="/admin/cars/edit/<?= $id ?>" method="post" class="needs-validation" novalidate>
                                <div class="form-group row">
                                    <label for="name" class="col-sm-2 col-form-label">Name:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="name" class="form-control-plaintext<?= (!empty($data['name_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['name'] ?>" id="name_field" readonly>
                                        <span class="invalid-feedback"><?= $data['name_err'] ?></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="plate_number" class="col-sm-2 col-form-label">Plate number:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="plate_number" class="form-control" value="<?= $data['plate_number'] ?>" id="plate_number_field">                                        
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="brand" class="col-sm-2 col-form-label">Brand:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="brand" class="form-control" value="<?= $data['brand'] ?>" id="brand">                                        
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="model" class="col-sm-2 col-form-label">Model:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="model" class="form-control" value="<?= $data['model'] ?>" id="model">                                        
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="year" class="col-sm-2 col-form-label">Year:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="year" class="form-control" value="<?= $data['year'] ?>" id="year">                                        
                                    </div>
                                </div>                                
                                <div class="mt-5">
                                    <input type="submit" value="Update Car" class="btn main-color-bg" id="register_button">
                                    <a href="/admin/cars/" class="btn btn-secondary">Back</a>
                                </div>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </main>
<div class="container">
<?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/footer.php'); ?>