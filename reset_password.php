<?php

    $pageDetails = ['title' => 'Reset Your Password'];
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');

    // Check for POST method
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        // process form

        // Sanitize POST data
        $email = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);

        // Init data
        $data = [
            'email' => trim($email),
            'email_err' => ''
        ];

        // Validate email
        if(empty($data['email'])) {
            $data['email_err'] = 'Please enter email';
        } elseif(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $data['email_err'] = 'Not a valid email address';
        }

        // Create tokens
        $selector = bin2hex(random_bytes(8));
        $token = random_bytes(32);

        $url = sprintf('%sreset.php?%s', URLROOT.'/', http_build_query([
            'selector' => $selector,
            'validator' => bin2hex($token)
        ]));

        // Create expiration token
        // $expires = new DateTime('NOW');
        // $expires->add(new DateInterval('PT01H')); // 1 
        
        // Create expiration token
        $date = date("Y-m-d H:i:s");
        // Convert date to unixtime and add 12 hours
        $expires = strtotime('+1 hour',strtotime($date));

        // Push new params to data
        $data += [
            'selector'  =>  $selector, 
            'token'     =>  hash('sha256', $token),
            'url'       =>  $url,
            'expires'   =>  $expires
        ];

        // Delete any existing tokens for this user
        deleteExistingToken($pdo, $data['email']);

        // Insert reset token into database
        insertResetToken($pdo, $data);

        // Send email
        passwordResetEmail($data);
        $msg->success('We processed your request.', '/reset_password.php');

    } else {
        // Init data
        $data = [
            'email' => '',
            'email_err' => ''
        ];

    }

    require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/header.php');
    
?>
    <div class="container">
    <div class="row mt-5">
        <div class="col-md-6 mx-auto">
            <div class="card card-body bg-light mt-5 mb-5">
            <?php $msg->display() ?>
                <h2>Reset Password</h2>
                <p>Please enter your email address. If the email is registered we will send a link to create a new password.</p>
                <form action="<?= htmlspecialchars(CURRPATH) ?>" method="post" class="needs-validation" novalidate>
                    <div class="form-group">
                        <label for="email">Email: <sup>*</sup></label>
                        <input type="text" name="email" class="form-control form-control <?= (!empty($data['email_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['email'] ?>">
                        <span class="invalid-feedback"><?= $data['email_err'] ?></span>
                    </div>
                     <div class="row">
                        <div class="col">
                        </div>
                        <div class="col">
                            <input type="submit" value="Reset Password" class="btn btn-block main-color-bg">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/footer.php'); ?>