<?php
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');

    if(isset($_GET['email']) && !empty($_GET['email']) AND isset($_GET['hash']) && !empty($_GET['hash'])){
        // Verify data
        $email = filter_var($_GET['email'], FILTER_SANITIZE_STRING); // Set email variable
        $hash = filter_var($_GET['hash'], FILTER_SANITIZE_STRING); // Set hash variable

        $matchedUser = matchUserEmailAndHash($pdo, $email, $hash);
        // ChromePhp::log('hello, console!');
        // ChromePhp::log($matchedUser);

        if($matchedUser[0] === true && $matchedUser[1]->expires >= time()){
            // We have a match, activate the account
            activateUser($pdo, $email, $hash);
            $msg->success('Your account has been activated, you can now login.');
            redirect('/login.php');
        } elseif($matchedUser[0] === true && $matchedUser[1]->expires < time()) {
            // We have a match, but activation time expired.
            //dd($matchedUser);
            deleteInactiveUser($pdo, $matchedUser[1]->email);
            $msg->error('Your activation link expired. Please register again.');
            redirect('/register.php');            
        } elseif($matchedUser[0] === false && $matchedUser[1] === 'active') {
            // Click on link when already active
            $msg->error('You already activated your account. This link has expired.');
            redirect('/login.php');
        } else {
            // No match -> invalid url or account has already been activated.
            $msg->error('The url is invalid, please use the link that has been sent to your email.');
            redirect('/login.php');
        }
    } else {
        // Invalid approach
        $msg->error('Invalid approach, please use the link that has been sent to your email.');
        redirect('/login.php');
    }