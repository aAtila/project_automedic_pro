<?php
    $pageDetails = [
        'title' => 'Create an account'
    ];

    $headerLinks = array(
        '<script src="https://www.google.com/recaptcha/api.js"></script>'
    );

    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');

    // Check for POST method
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        // process form

        // Sanitize POST data
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        // Init data
        $data = [
            'name' => trim($_POST['name']),
            'email' => trim($_POST['email']),
            'password' => trim($_POST['password']),
            'confirm_password' => trim($_POST['confirm_password']),
            'recaptcha_response' => trim($_POST['g-recaptcha-response']),
            'name_err' => '',
            'email_err' => '',
            'password_err' => '',
            'confirm_password_err' => '',
            'recaptcha_response_err' => ''
        ];

        // Validate email
        if(empty($data['email'])) {
            $data['email_err'] = 'Please enter email';
        } elseif(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $data['email_err'] = 'Not a valid email address';
        } else {
            //Check email
            $user = findUserByEmail($pdo, $data['email']);
            if($user[0] === true) {
                $data['email_err'] = 'Email is already taken';
            }
        }

        // Validate name
        if(empty($data['name'])) {
            $data['name_err'] = 'Please enter name';
        }

        // Validate password
        if(empty($data['password'])) {
            $data['password_err'] = 'Please enter password';
        } elseif(strlen($data['password']) < 6) {
            $data['password_err'] = 'Password must be at least 6 characters';
        }

        // Validate confirm password
        if(empty($data['confirm_password'])) {
            $data['confirm_password_err'] = 'Please confirm password';
        } else {
            if($data['password'] != $data['confirm_password']) {
                $data['confirm_password_err'] = 'Passwords do not match';
            }
        }

        // captcha
        if(isset($data['recaptcha_response']) && !empty($data['recaptcha_response'])) {
            //your site secret key
            $secret = '6LepnWIUAAAAALcT6b87FFPB6XjHoOoENag8hJmA';
            //get verify response data
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$data['recaptcha_response']);
            $responseData = json_decode($verifyResponse);
            if($responseData->success) {
                //captacha validated successfully.
            } else {
                $data['recaptcha_response_err'] = 'Robot verification failed, please try again.';
            }      
        } else {
            $data['recaptcha_response_err'] = 'Please complete captcha.';
            $msg->error('Invalid captcha.');
        }

        // Make sure errors are empty
        if(empty($data['email_err']) && empty($data['name_err']) && empty($data['password_err']) && empty($data['confirm_password_err']) && empty($data['recaptcha_response_err'])) {
            // Validated
            
            // Hash password
            $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

            // Generate the activation hash
            $data['hash'] = md5(rand(0,1000));

            // Create expiration token
            $date = date("Y-m-d H:i:s");
            // Convert date to unixtime and add 12 hours
            $expires = strtotime('+12 hours',strtotime($date));
            $data['expires'] = $expires;

            // Register user
            if(registerUser($pdo, $data)) {
                if(sendVerificationEmail($data)) {
                $msg->success('Your account has been created but isn\'t active yet. Please activate it by clicking the activation link that has been sent to your email address.', '/login.php');
                } else {
                    die('Ouch.. something went wrong');
                }
            } else {
                die('Something went wrong');
            }

        }

    } else {
        // Init data
        $data = [
            'name' => '',
            'email' => '',
            'password' => '',
            'recaptcha_response' => '',
            'confirm_password' => '',
            'name_err' => '',
            'email_err' => '',
            'password_err' => '',
            'confirm_password_err' => '',
            'recaptcha_response_err' => ''
        ];
    }

    require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/header.php');
?>
    <div class="container">
    <div class="row mt-5">
        <div class="col-md-6 mx-auto">
            <div class="card card-body bg-light mt-5 mb-5">
            <?php $msg->display() ?>
                <h2>Create An Account</h2>
                <p>Please fill out this form to register with us.</p>
                <form action="<?= htmlspecialchars(CURRPATH) ?>" method="post" class="needs-validation" novalidate>
                    <div class="form-group">
                        <label for="name">Name: <sup>*</sup></label>
                        <input type="text" name="name" class="form-control <?= (!empty($data['name_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['name'] ?>" id="name_field">
                        <span class="invalid-feedback"><?= $data['name_err'] ?></span>
                    </div>
                    <div class="form-group">
                        <label for="email">Email: <sup>*</sup></label>
                        <input type="text" name="email" class="form-control <?= (!empty($data['email_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['email'] ?>" id="email_field">
                        <span class="invalid-feedback"><?= $data['email_err'] ?></span>
                    </div>
                    <div class="form-group">
                        <label for="password">Password: <sup>*</sup></label>
                        <input type="password" name="password" class="form-control <?= (!empty($data['password_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['password'] ?>">
                        <span class="invalid-feedback"><?= $data['password_err'] ?></span>
                    </div>
                    <div class="form-group">
                        <label for="confirm_password">Confirm Password: <sup>*</sup></label>
                        <input type="password" name="confirm_password" class="form-control <?= (!empty($data['confirm_password_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['confirm_password'] ?>">
                        <span class="invalid-feedback"><?= $data['confirm_password_err'] ?></span>
                    </div>
                    <div class="form-group">
                        <div class="g-recaptcha" data-sitekey="6LepnWIUAAAAADtbsGIBR29zKn3uIoaOG31m-ZQF"></div>
                        <p class="text-danger"><?= $data['recaptcha_response_err'] ?></p>
                    </div>
                    <div class="row">
                        <div class="col">
                            <input type="submit" value="Register" class="btn btn-block main-color-bg" id="register_button">
                        </div>
                        <div class="col">
                            <a href="login.php" class="btn btn-light btn-block">Have an account? Login</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/footer.php'); ?>