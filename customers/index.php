<?php
    $pageDetails = [
        'tagline' => 'manage your vehicle',
        'title' => 'Manage Vehicles',
        'breadcrumb' => array()
    ];
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/customers/form_validation.php');
    require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/customers/header.php');
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/customers/navigation.php');
?>
    <main>
        <div class="container">
            <div class="row">
                <!-- Sidebar -->
                <div class="col-md-3 d-none d-md-block">
                    <?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/customers/sidebar.php'); ?>
                </div>
                <div class="col-md-9">
                <?= $msg->display(); ?>
                   <!-- Shop Overview -->
                <div class="card mb-4">
                    <div class="card-header main-color-bg">Options</div>
                        <div class="card-body">
                            <div class="card-deck">
                                <div class="col-md-4">
                                    <div class="card bg-light text-center h-100 shadow-sm">
                                        <div class="card-body clickable-element" id="check-status-btn">
                                            <h2><i class="fas fa-search text-main"></i></h2>
                                            <h5><strong>Check Status</strong></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card bg-light text-center shadow-sm">
                                        <div class="card-body clickable-element" id="submit-problem-btn">
                                            <h2><i class="far fa-share-square text-main"></i></h2>
                                            <h5><strong>Submit Problem</strong></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card bg-light text-center shadow-sm">
                                        <div class="card-body clickable-element" id="new-vehicle-btn">
                                            <h2><i class="fas fa-plus text-main"></i></h2>
                                            <h5><strong>Add New Vehicle</strong></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="card mb-4 hide-me" id="check-status-btn-card">
                    <div class="card-header main-color-bg">Check The Current Status of Your Vehicle</div>
                        <div class="card-body">
                            <form>
                            <div class="form-row align-items-center justify-content-center mt-2 mb-2">
                                <div class="col-auto">
                                <label class="sr-only" for="inlineFormInputGroup">Plate number</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-search"></i></div>
                                    </div>
                                    <input type="text" class="form-control form-control" id="plateNumberInput" placeholder="Plate number..">
                                </div>
                                </div>
                                <div class="col-auto">
                                <button type="submit" class="btn main-color-bg mb-2">Look Up</button>
                                </div>
                            </div>
                            </form>
                        </div>
                </div>
                <div id="results"></div>
            </div>
        </div>
    </main>

<!-- MODALS -->

    <!-- Check Vehicle Status Modal -->
    <div class="modal fade" id="check-status-modal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-body">
            <div class="mb-1">
                <button type="button" class="close-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <h3 class="text-center mb-5"><i class="fas fa-search text-main"></i> Check Current Status</h3>
            <p class="text-center mb-4">Enter the plate number of your vehicle to get its current status.</p>
            <form class="pr-4 pl-4 needs-validation" id="search-status-form" action="process_status.php" method="POST" novalidate>
            <div class="form-group">
                <input type="text" name="plate_number_field" class="form-control" placeholder="Plate number" id="plate-number-field">
            </div>
            <div class="row">
                <div class="col"><input type="submit" value="Look Up" class="btn btn-block main-color-bg"></div>
                <div class="col"><button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Close</button></div>
            </div>
            </form>
        </div>
        </div>
    </div>
    </div>

    <!-- No Car Registered Modal -->
    <div class="modal fade" id="no-car-reg-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="plate-number-modal-title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" id="plate-number-modal-content">
        <div class="modal-body" id="plate-number-modal-body">
            <h3 class="text-center mb-4"><i class="fas fa-exclamation-circle text-danger"></i> No Car Registered</h3>
            <p class="mb-4">You don't have any cars registered with us. Please add at least one to continue with our services.</p>
            <!-- Include Form -->
            <?php include ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/customers/forms/form_new_vehicle.php'); ?>
        </div>
        </div>
    </div>
    </div>

    <!-- Submit Problem Modal -->
    <div class="modal fade" id="submit-problem-modal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="SubmitNewProblemModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-body">
            <div class="mb-1">
                <button type="button" class="close-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <h3 class="text-center mb-5"><i class="far fa-share-square text-main"></i> Submit Problem</h3>
            <p class="mb-4 text-center">Fill out the form below to submit your problem.</p>
            <?php include ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/customers/forms/form_submit_problem.php'); ?>
        </div>
        </div>
    </div>
    </div>

    <!-- Add New Vehicle Modal -->
    <div class="modal fade" id="new-vehicle-modal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-body">
            <div class="mb-1">
                <button type="button" class="close-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <h3 class="text-center mb-5"><i class="fas fa-plus text-main"></i> Add New Vehicle</h3>
            <p class="mb-4 text-center">Fill out the form below to add your new vehicle to our system.</p>
            <?php include ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/customers/forms/form_new_vehicle.php'); ?>
        </div>
        </div>
    </div>
    </div>    

<?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/footer.php'); ?>

<!-- Call .js Files -->
<script>
    $('#select-services').multiselect({
        buttonWidth: '100%',
        maxHeight: 400,
        
    });
</script>
<script src="/js/customers.js"></script>

<?= $data_nv['show-modal'] ?>
<?= $data_sp['show-modal'] ?>

<!-- Load No Car Registered Modal -->
<?php
    if(!userHasCar($pdo, $_SESSION['user_id'])) {
        echo '<script src="/js/no_car.js"></script>';
    };
?>