<?php
    $pageDetails = [
        'tagline' => 'reservations',
        'title' => 'Reservation',
        'breadcrumb' => array('Reservation')
    ];
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
    require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/customers/reservations/header.php');
    require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/customers/navigation.php');

    // Check if the visitor comes by filling the form
    if(!isset($_COOKIE['reservations'])) {
        redirect('/customers/');
    }
    
    //setcookie('reservations', $_SESSION['car_id'] . "." . '60', time()+3600, '/customers/');    
    // var_dump($_COOKIE['reservations']);
    // var_dump($_SESSION['car_id']);
    // var_dump($_SESSION['user_id']);
    // var_dump($_SESSION['duration']);
    // var_dump('lastId', $_SESSION['lastId']);
    
//    require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/customers/reservations/header.php');
  
?>
    <main>
        <div class="container">
            <div class="row">
                <!-- Sidebar -->
                <div class="col-md-3 d-none d-md-block">
                    <?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/customers/sidebar.php'); ?>
                </div>
                <div class="col-md-9">
                <div class="d-none d-md-block">
                    <?= $msg->display(); ?>
                </div>
                    <!-- Calendar -->
                    <div class="card mb-4">
                        <div class="card-header main-color-bg">Calendar</div>
                            <div class="card-body">
                                <div id="calendar" class="p-3"></div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- Call .js Files -->
<script src="/js/reservations_cal.js"></script>

<!-- Load No Car Registered Modal -->
<?php
    if(!userHasCar($pdo, $_SESSION['user_id'])) {
        echo '<script src="/js/no_car.js"></script>';
    };
?>