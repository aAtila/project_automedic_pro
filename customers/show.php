<?php
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
    require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/users/header.php');

    if(isset($_GET['id']) && !empty($_GET['id']) && CTYPE_DIGIT($_GET['id'])) {
        $id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT); // Set email variable
        $user = getSingleUser($pdo, $id);
        ?>
        <ul>
        <li><?= $user->name ?></li>
        <li><?= $user->email ?></li>
        <li><?= ($user->active === 1) ? 'Active' : 'Inactive'; ?></li>
        <li><?= $user->role ?></li>
        <li><?= $user->created_at ?></li>
        </ul>
        <a href="/users/edit.php?id=<?= $user->userId ?>"><button type="button" class="btn btn-primary">Edit</button></a>

    <?php } else {
        // redirect('/users/');
        echo 'better luck next time';
    }
?>