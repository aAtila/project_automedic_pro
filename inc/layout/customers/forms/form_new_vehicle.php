<form class="pr-4 pl-4 needs-validation" id="new-vehicle-form" action="/customers/" method="POST" novalidate>
    <div class="form-group">
        <input type="text" name='plate_number' class="form-control <?= (!empty($data_nv['plate_number_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data_nv['plate_number'] ?>" placeholder="Plate number">
        <span class="invalid-feedback"><?= $data_nv['plate_number_err'] ?></span>
    </div>
    <div class="form-group">
        <input type="text" name='brand' class="form-control <?= (!empty($data_nv['brand_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data_nv['brand'] ?>" placeholder="Brand">
        <span class="invalid-feedback"><?= $data_nv['brand_err'] ?></span>
    </div>
    <div class="form-group">
        <input type="text" name='model' class="form-control <?= (!empty($data_nv['model_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data_nv['model'] ?>" placeholder="Model">
        <span class="invalid-feedback"><?= $data_nv['model_err'] ?></span>
    </div>
    <div class="form-group">
        <input type="text" name='year' class="form-control <?= (!empty($data_nv['year_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data_nv['year'] ?>" placeholder="Year">
        <span class="invalid-feedback"><?= $data_nv['year_err'] ?></span>
    </div>
    <input type="hidden" name="form_name" readonly value="new-vehicle-form">
    <div class="row">
        <div class="col"><input type="submit" value="Submit" class="btn btn-block main-color-bg"></div>
        <div class="col"><button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Close</button></div>
    </div>
</form>