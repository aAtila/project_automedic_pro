<?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    
    require ($_SERVER["DOCUMENT_ROOT"] . '/inc/PHPMailer/src/Exception.php');
    require ($_SERVER["DOCUMENT_ROOT"] . '/inc/PHPMailer/src/PHPMailer.php');
    require ($_SERVER["DOCUMENT_ROOT"] . '/inc/PHPMailer/src/SMTP.php');

    function sendVerificationEmail($data) {

        $email = $data['email'];
        $hash = $data['hash'];

        $mail = new PHPMailer;

        //Server settings
        // $mail->SMTPDebug = 3; // Enable verbose debug output
        $mail->isSMTP(); // Set mailer to use SMTP
        $mail->Host = SMTP_HOST; // Specify main and backup SMTP servers
        $mail->SMTPAuth = true; // Enable SMTP authentication
        $mail->Username = SMTP_USER; // SMTP username
        $mail->Password = SMTP_PASS; // SMTP password
        $mail->SMTPSecure = SMTP_ENCRYPT; // Enable TLS encryption, `ssl` also accepted
        $mail->Port = SMTP_PORT; // TCP port to connect to
    
        //Recipients
        $mail->setFrom(VERIFICATION_EMAIL, SITENAME);
        $mail->addAddress($email);     // Add a recipient
        // $mail->addReplyTo('info@example.com', 'Information');
    
        //Content
        $mail->isHTML(true); // Set email format to HTML
        $mail->Subject = 'Account Verification Link';
        $mail->Body    = '

        Thanks for signing up!
        <br><br>
        Your account has been created, you can login with the following credentials after you have activated your account by visiting the url below.
        <br><br>
        ------------------------
        <br>
        Email: '.$email.'
        <br>
        Password: the one you set at the time of registration
        <br>
        ------------------------
        <br><br>
        Please click this link to activate your account:
        <br>
        <a href="'. URLROOT .'/verify.php?email='.$email.'&hash='.$hash.'">'. URLROOT .'/verify.php?email='.$email.'&hash='.$hash.'</a>
        
        ';

        // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        
        if(!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            return true;
        }
    }

    function passwordResetEmail($data) {

        $email = $data['email'];
        $url = $data['url'];

        $mail = new PHPMailer;

        //Server settings
        // $mail->SMTPDebug = 3; // Enable verbose debug output
        $mail->isSMTP(); // Set mailer to use SMTP
        $mail->Host = SMTP_HOST; // Specify main and backup SMTP servers
        $mail->SMTPAuth = true; // Enable SMTP authentication
        $mail->Username = SMTP_USER; // SMTP username
        $mail->Password = SMTP_PASS; // SMTP password
        $mail->SMTPSecure = SMTP_ENCRYPT; // Enable TLS encryption, `ssl` also accepted
        $mail->Port = SMTP_PORT; // TCP port to connect to
    
        //Recipients
        $mail->setFrom(VERIFICATION_EMAIL, SITENAME);
        $mail->addAddress($email);     // Add a recipient
        // $mail->addReplyTo('info@example.com', 'Information');
    
        //Content
        $mail->isHTML(true); // Set email format to HTML
        $mail->Subject = 'Your password reset link';

        // Message
        $mail->Body = '<p>We recieved a password reset request. The link to reset your password is below. ';
        $mail->Body .= 'If you did not make this request, you can ignore this email</p>';
        $mail->Body .= '<p>Here is your password reset link:</br>';
        $mail->Body .= sprintf('<a href="%s">%s</a></p>', $url, $url);
        $mail->Body .= '<p>Thanks!</p>';

        // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        
        if(!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        }
    }

    function sendContactMail($data) {

        $mail = new PHPMailer;

        //Server settings
        // $mail->SMTPDebug = 3; // Enable verbose debug output
        $mail->isSMTP(); // Set mailer to use SMTP
        $mail->Host = SMTP_HOST; // Specify main and backup SMTP servers
        $mail->SMTPAuth = true; // Enable SMTP authentication
        $mail->Username = SMTP_USER; // SMTP username
        $mail->Password = SMTP_PASS; // SMTP password
        $mail->SMTPSecure = SMTP_ENCRYPT; // Enable TLS encryption, `ssl` also accepted
        $mail->Port = SMTP_PORT; // TCP port to connect to
    
        //Recipients
        $mail->setFrom(VERIFICATION_EMAIL, SITENAME);
        $mail->addAddress(ADMIN_EMAIL);     // Add a recipient
        $mail->addReplyTo($data['email'], $data['name']);
    
        //Content
        $mail->isHTML(true); // Set email format to HTML
        $mail->Subject = 'New Contact Form Submission';

        // Message
        $mail->Body = '<p><strong>Name: </strong>'.$data['name'].'</p>';
        $mail->Body .= '<p><strong>Email: </strong>'.$data['email'].'</p>';
        $mail->Body .= '<p><strong>Subject: </strong>'.$data['subject'].'</p>';
        $mail->Body .= '<p><strong>Message: </strong>'.$data['message'].'</p>';
        $mail->Body .= '<hr>';
        $mail->Body .= '<p><strong>IP Address: </strong>'.$data['ip_address'].'</p>';
        $mail->Body .= '<p><strong>Browser: </strong>'.$data['browser'].'</p>';

        // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        
        if(!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            return true;
        }
    }

    function newPendingJobEmail($data) {

        $mail = new PHPMailer;

        //Server settings
        // $mail->SMTPDebug = 3; // Enable verbose debug output
        $mail->isSMTP(); // Set mailer to use SMTP
        $mail->Host = SMTP_HOST; // Specify main and backup SMTP servers
        $mail->SMTPAuth = true; // Enable SMTP authentication
        $mail->Username = SMTP_USER; // SMTP username
        $mail->Password = SMTP_PASS; // SMTP password
        $mail->SMTPSecure = SMTP_ENCRYPT; // Enable TLS encryption, `ssl` also accepted
        $mail->Port = SMTP_PORT; // TCP port to connect to
    
        //Recipients
        $mail->setFrom(VERIFICATION_EMAIL, SITENAME);
        $mail->addAddress(ADMIN_EMAIL);     // Add a recipient
    
        //Content
        $mail->isHTML(true); // Set email format to HTML
        $mail->Subject = 'New Pending Job!';

        // Message
        $mail->Body = '<p><strong>Name: </strong>'.$data['name'].'</p>';
        $mail->Body .= '<p><strong>Car: </strong>'.$data['plate_number'].'</p>';
        $mail->Body .= '<p><strong>Services: </strong><ul><li>'.implode('</li><li>', $data['services']).'</li></ul></p>';
        $mail->Body .= '<p><strong>Description: </strong>'.$data['desc'].'</p>';
        $mail->Body .= '<hr>';
        $mail->Body .= '<p><strong>IP Address: </strong>'.$data['ip_address'].'</p>';
        $mail->Body .= '<p><strong>Browser: </strong>'.$data['browser'].'</p>';

        // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        
        if(!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            return true;
        }
    }

    function messageToCustomer($data) {

        $mail = new PHPMailer;

        //Server settings
        // $mail->SMTPDebug = 3; // Enable verbose debug output
        $mail->isSMTP(); // Set mailer to use SMTP
        $mail->Host = SMTP_HOST; // Specify main and backup SMTP servers
        $mail->SMTPAuth = true; // Enable SMTP authentication
        $mail->Username = SMTP_USER; // SMTP username
        $mail->Password = SMTP_PASS; // SMTP password
        $mail->SMTPSecure = SMTP_ENCRYPT; // Enable TLS encryption, `ssl` also accepted
        $mail->Port = SMTP_PORT; // TCP port to connect to
    
        //Recipients
        $mail->setFrom(VERIFICATION_EMAIL, SITENAME);
        $mail->addAddress($data['email']);     // Add a recipient
        $mail->addReplyTo(ADMIN_EMAIL, 'AutoMedic Team');
    
        //Content
        $mail->isHTML(true); // Set email format to HTML
        $mail->Subject = $data['subject'];

        // Message
        $mail->Body = '<p>'.$data['message'].'</p>';
        $mail->Body .= '<hr>';
        $mail->Body .= '<p>Sent from the AutoMedic Auto Care Platform. Car plate number: <strong>'.$data['car'].'</strong></p>';

        // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        
        if(!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            return true;
        }
    }

    function sendDeclineMessage($data) {

        $mail = new PHPMailer;

        //Server settings
        // $mail->SMTPDebug = 3; // Enable verbose debug output
        $mail->isSMTP(); // Set mailer to use SMTP
        $mail->Host = SMTP_HOST; // Specify main and backup SMTP servers
        $mail->SMTPAuth = true; // Enable SMTP authentication
        $mail->Username = SMTP_USER; // SMTP username
        $mail->Password = SMTP_PASS; // SMTP password
        $mail->SMTPSecure = SMTP_ENCRYPT; // Enable TLS encryption, `ssl` also accepted
        $mail->Port = SMTP_PORT; // TCP port to connect to
    
        //Recipients
        $mail->setFrom(VERIFICATION_EMAIL, SITENAME);
        $mail->addAddress($data['email']);     // Add a recipient
        $mail->addReplyTo(ADMIN_EMAIL, 'AutoMedic Team');
    
        //Content
        $mail->isHTML(true); // Set email format to HTML
        $mail->Subject = 'Submission Declined | AutoMedic';

        // Message
        $mail->Body = '<p>We regret to inform you that we could not accept your submission this time. The reason for making such decision was the following:</p>';
        $mail->Body .= '<p><i>"'.$data['decline_reason'].'"</i></p>';
        $mail->Body .= '<p>Kind regards,<br>The AutoMedic Team</p>';

        // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        
        if(!$mail->send()) {
            return false;
        } else {
            return true;
        }
    }