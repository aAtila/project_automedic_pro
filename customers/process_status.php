<?php

    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
    // Check if $_POST is set
    if (empty($_POST['plate_number'])) {
    redirect('/404.php');
    exit;
    }

    // Init data
    $data = [
        'plate_number' => strtoupper(preg_replace('/[^a-zA-Z0-9]/', '', $_POST['plate_number'])),
        'user_id' => $_SESSION['user_id']
    ];

    $carDetails = itsUsersCar($pdo, $data);
    //ChromePhp::log('test1', $carDetails);
    // Check if user owns the car
  

    if(!$carDetails) {
        echo '
        <div class="alert alert-danger animated flash" role="alert">
        You don\'t have a car registered with that plate number. Try again.
        </div>
        ';
    } elseif($carDetails[0]) {
        //ChromePhp::log('car details', $carDetails);
        $carId = $carDetails[1]->id;
        // Get status
        $carStatus = getVehicleStatus($pdo, $data, $carId);
        ChromePhp::log('car status', $carStatus);
        // Check if the car is on service
        if(!$carStatus) {
            echo '
            <div class="alert alert-info animated flash" role="alert">
            The car with that plate number isn\'t on service.
            </div>
            ';
        } else {
            //header('Content-type: text/JSON');
            echo json_encode($carStatus);
        }
    }