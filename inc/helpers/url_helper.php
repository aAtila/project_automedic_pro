<?php
    // Simple page redirect
    function redirect($page) {
        header('location: '. $page);
    }

    function forceRedirect($url = '/'){
        if(!headers_sent()) {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location:'.$url);  
            header('Connection: close');
            exit;
        }
        else {
            echo 'location.replace('.$url.');';
        }
        exit;
    }