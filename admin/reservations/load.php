<?php     
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
    
    if(!isLoggedIn()) {
        $msg->error('You need to be logged in to access this page.', '/login.php');
    } elseif(isUser()) {
        $msg->error('You don\'t have permissions to access that page.', '/customers/');
    }
    
    //load.php
        //$connect = new PDO('mysql:host=localhost; dbname=automedic', 'root', '123456');
        $data = array();
        $query = "SELECT * FROM jobs";
        $statement = $connect->prepare($query);
        $statement->execute();
        $result = $statement->fetchAll();        

        foreach($result as $row)
        {
            $data[] = array(
                'id'   => $row["id"],                
                'cars_id' => $row["car_id"],
                'title' => 'CarID: ' . $row['car_id'],
                'start' => $row["reservation_start"],
                'end' => $row["reservation_end"]        
            );
        }
        echo json_encode($data);
?>