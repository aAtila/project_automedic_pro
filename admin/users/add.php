<?php
    $pageDetails = [
        'tagline' => 'manage your users',
        'title' => 'Add New User'
    ];

    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');

    // Get user roles for the form
    $userRoles = getUserRoles($pdo);

    // Check for POST method
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        // process form
        
        // Sanitize POST data
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        // Init data
        $data = [
            'name' => trim($_POST['name']),
            'email' => trim($_POST['email']),
            'password' => trim($_POST['password']),
            'confirm_password' => trim($_POST['confirm_password']),
            'role' => trim($_POST['role']),
            'name_err' => '',
            'email_err' => '',
            'password_err' => '',
            'confirm_password_err' => '',
            'role_err' => ''
        ];

        // Validate email
        if(empty($data['email'])) {
            $data['email_err'] = 'Please enter email';
        } elseif(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $data['email_err'] = 'Not a valid email address';
        } else {
            //Check email
            $user = findUserByEmail($pdo, $data['email']);
            if($user[0] === true) {
                $data['email_err'] = 'Email is already taken';
            }
        }

        // Validate name
        if(empty($data['name'])) {
            $data['name_err'] = 'Please enter name';
        }

        // Validate password
        if(empty($data['password'])) {
            $data['password_err'] = 'Please enter password';
        } elseif(strlen($data['password']) < 6) {
            $data['password_err'] = 'Password must be at least 6 characters';
        }

        // Validate confirm password
        if(empty($data['confirm_password'])) {
            $data['confirm_password_err'] = 'Please confirm password';
        } else {
            if($data['password'] != $data['confirm_password']) {
                $data['confirm_password_err'] = 'Passwords do not match';
            }
        }

        // Validate role
        if(empty($data['role'])) {
            $data['role_err'] = 'Please choose role';
        }

        // Make sure errors are empty
        if(empty($data['email_err']) && empty($data['name_err']) && empty($data['password_err']) && empty($data['confirm_password_err']) && empty($data['role_err'])) {
            // Validated

            // Hash password with salt
            $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

            // Register user
            if(adminAddNewUser($pdo, $data)) {
                $msg->success('New user added.', '/admin/users/');
            } else {
                die('Something went wrong');
            }

        }

    } else {
        // Init data
        $data = [
            'name' => '',
            'email' => '',
            'password' => '',
            'confirm_password' => '',
            'role' => '',
            'name_err' => '',
            'email_err' => '',
            'password_err' => '',
            'confirm_password_err' => '',
            'role_error' => ''
        ];
    }

    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/header.php');
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/navigation.php');

?>

    <main>
        <div class="container">
            
            <div class="row">
                <!-- Sidebar -->
                <div class="col-md-3 d-none d-md-block">
                    <?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/sidebar.php'); ?>
                </div>
                <div class="col-md-9">
                <!-- Users overview-->
                <div class="card">
                    <div class="card-header main-color-bg"><i class="far fa-user"></i> Add new user</div>
                        <div class="card-body">
                            <!-- Show flash message -->
                            <?php $msg->display() ?>
                            <form action="/admin/users/add" method="post" class="needs-validation" novalidate>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">Name: <sup>*</sup></label>
                                            <input type="text" name="name" class="form-control <?= (!empty($data['name_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['name'] ?>" id="name_field">
                                            <span class="invalid-feedback"><?= $data['name_err'] ?></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email">Email: <sup>*</sup></label>
                                            <input type="text" name="email" class="form-control <?= (!empty($data['email_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['email'] ?>" id="email_field">
                                            <span class="invalid-feedback"><?= $data['email_err'] ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="password">Password: <sup>*</sup></label>
                                            <input type="password" name="password" class="form-control <?= (!empty($data['password_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['password'] ?>">
                                            <span class="invalid-feedback"><?= $data['password_err'] ?></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="confirm_password">Confirm Password: <sup>*</sup></label>
                                            <input type="password" name="confirm_password" class="form-control <?= (!empty($data['confirm_password_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['confirm_password'] ?>">
                                            <span class="invalid-feedback"><?= $data['confirm_password_err'] ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="role">Role: <sup>*</sup></label>
                                            <select name="role" class="form-control <?= (!empty($data['role_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['role'] ?>" id="role-select">
                                                <option value="" selected>Choose...</option>
                                                <?php foreach($userRoles as $role) : ?>
                                                <option value="<?= $role->id ?>"><?= ucwords($role->role) ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <span class="invalid-feedback"><?= $data['role_err'] ?></span>
                                        </div>
                                    </div>
                                </div>
                                <input type="submit" value="Add New User" class="btn main-color-bg" id="register_button">
                                <a href="/admin/users/" class="btn btn-secondary">Back</a>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </main>

<?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/footer.php'); ?>