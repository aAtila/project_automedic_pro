<?php
    $host = DB_HOST;
    $user = DB_USER;
    $pass = DB_PASS;
    $dbname = DB_NAME;

    // Error handling
    set_exception_handler(function($e) {
        error_log($e->getMessage());
        // echo "Error: " . $e->getMessage() . "</br></br>";
        exit('Something went wrong with the database. Check error_log for more details.'); //something a user can understand
    });

    // Set DSN
    $dsn = 'mysql:host=' . $host . ';dbname=' . $dbname;

    $options = [
        PDO::ATTR_EMULATE_PREPARES   => false, // turn off emulation mode for "real" prepared statements
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, //turn on errors in the form of exceptions
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ //make the default fetch be an object
    ];

    // Create a PDO instance
    $pdo = new PDO($dsn, $user, $pass, $options);

    // FullCalendar
    $connect = new PDO($dsn, $user, $pass);